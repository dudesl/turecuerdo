-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-05-2012 a las 15:48:13
-- Versión del servidor: 5.5.22-0ubuntu1
-- Versión de PHP: 5.3.10-1ubuntu3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `turecuerdo`
--
DROP DATABASE `turecuerdo`;
CREATE DATABASE `turecuerdo` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `turecuerdo`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion_tarea`
--

DROP TABLE IF EXISTS `asignacion_tarea`;
CREATE TABLE IF NOT EXISTS `asignacion_tarea` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `tarea_id` int(11) NOT NULL,
  `evento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `asignacion_unica` (`usuario_id`,`tarea_id`,`evento_id`),
  KEY `evento_id` (`evento_id`),
  KEY `tarea_id` (`tarea_id`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

DROP TABLE IF EXISTS `evento`;
CREATE TABLE IF NOT EXISTS `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lugar` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `cantidad_invitados` int(11) unsigned NOT NULL,
  `costo` double unsigned NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lugaryfecha_unico` (`lugar`,`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento_has_servicio`
--

DROP TABLE IF EXISTS `evento_has_servicio`;
CREATE TABLE IF NOT EXISTS `evento_has_servicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `evento_id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `eventoservicioUnique` (`evento_id`,`servicio_id`),
  KEY `servicioIndice` (`servicio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidad`
--

DROP TABLE IF EXISTS `habilidad`;
CREATE TABLE IF NOT EXISTS `habilidad` (
  `id` int(11) NOT NULL COMMENT 'identificador de habilidad',
  `habilidad` varchar(100) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'habilidad',
  PRIMARY KEY (`id`),
  UNIQUE KEY `habilidad` (`habilidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `habilidad`
--

INSERT INTO `habilidad` (`id`, `habilidad`) VALUES
(1, 'habilidad 1'),
(10, 'habilidad 10'),
(11, 'habilidad 11'),
(2, 'habilidad 2'),
(3, 'habilidad 3'),
(4, 'habilidad 4'),
(5, 'habilidad 5'),
(6, 'habilidad 6'),
(7, 'habilidad 7'),
(8, 'habilidad 8'),
(9, 'habilidad 9');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `has_habilidad`
--

DROP TABLE IF EXISTS `has_habilidad`;
CREATE TABLE IF NOT EXISTS `has_habilidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador de la relacion',
  `usuario_id` int(11) NOT NULL COMMENT 'identificador del usuario',
  `habilidad_id` int(11) NOT NULL COMMENT 'identificador de la habilidad',
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`),
  KEY `habilidad_id` (`habilidad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='habilidades que poseen los usuarios (empleados)' AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

DROP TABLE IF EXISTS `servicio`;
CREATE TABLE IF NOT EXISTS `servicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `servicio_unico` (`nombre`,`descripcion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=15 ;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_has_tarea`
--

DROP TABLE IF EXISTS `servicio_has_tarea`;
CREATE TABLE IF NOT EXISTS `servicio_has_tarea` (
  `id_servicio` int(11) NOT NULL DEFAULT '0',
  `id_tarea` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_servicio`,`id_tarea`),
  KEY `id_tarea` (`id_tarea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;


-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla `tarea`
--

DROP TABLE IF EXISTS `tarea`;
CREATE TABLE IF NOT EXISTS `tarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador de la tarea',
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre de la tarea',
  `descripcion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'descripción de la tarea (opcional)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tarea_unica` (`nombre`,`descripcion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificacion del usuario',
  `username` varchar(100) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre de usuario',
  `password` varchar(100) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'contraseña',
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'nombre del empleado',
  `direccion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'direccion del empleado (opcional)',
  `telefono` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'telefono del empleado ',
  `mail` varchar(100) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'mail del telefono',
  `rol` enum('Administrador','Empleado') COLLATE utf8_spanish2_ci NOT NULL COMMENT 'identificador del rol en el sistema del usuario',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='tabla de usuarios' AUTO_INCREMENT=12 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asignacion_tarea`
--
ALTER TABLE `asignacion_tarea`
  ADD CONSTRAINT `asignacion_tarea_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `asignacion_tarea_ibfk_2` FOREIGN KEY (`tarea_id`) REFERENCES `tarea` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `asignacion_tarea_ibfk_3` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `evento_has_servicio`
--
ALTER TABLE `evento_has_servicio`
  ADD CONSTRAINT `evento_has_servicio_ibfk_1` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `evento_has_servicio_ibfk_2` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `has_habilidad`
--
ALTER TABLE `has_habilidad`
  ADD CONSTRAINT `has_habilidad_ibfk_5` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `has_habilidad_ibfk_6` FOREIGN KEY (`habilidad_id`) REFERENCES `habilidad` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `servicio_has_tarea`
--
ALTER TABLE `servicio_has_tarea`
  ADD CONSTRAINT `servicio_has_tarea_ibfk_1` FOREIGN KEY (`id_servicio`) REFERENCES `servicio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_has_tarea_ibfk_2` FOREIGN KEY (`id_tarea`) REFERENCES `tarea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `username`, `password`, `nombre`, `direccion`, `telefono`, `mail`, `rol`) VALUES
(1, 'dudesl', '900c736fa958e781fe4652e2dd810e02', 'Santiago Barchetta', 'dudesl@gmail.com', '2664850949', 'dudesl@gmail.com', 'Administrador'),

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
