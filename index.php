<?php

// Setea constastes del sistema.
define('DOCROOT', realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
define('DEFAULTCONTROLLER', 'Login');
define('DEFAULTACTION', 'index');

include_once('bootstrap.php');

//Formamos el nombre del Controlador o en su defecto, tomamos que es el IndexController

$getValues = array_keys($_GET);
if (!empty($getValues[0]))
    $controllerName = ucfirst($getValues[0]) . 'Controller';
else
    $controllerName = DEFAULTCONTROLLER . 'Controller';

//Lo mismo sucede con las acciones, si no hay accion, tomamos index como accion
if (!empty($getValues[1]))
    $actionName = $getValues[1];
else
    $actionName = DEFAULTACTION;

$actionParams = null;
if (count($_GET) > 2) {
    $countParams = count($_GET);
    for ($i = 2; $i <= count($_GET) - 1; $i++) {
        $actionParams[] = $getValues[$i];
    }
}

$controllerPath = $config->get('controllersFolder') . DIRECTORY_SEPARATOR . $controllerName . '.php';

//Incluimos el fichero que contiene nuestra clase controladora solicitada	
if (is_file($controllerPath))
    require_once $controllerPath;
else {
    header("Location:?error&e404");
    return;
}


//Si no existe la accion que buscamos o no es accesible, retornamos la accion por defecto
if (is_callable(array($controllerName, $actionName)) == false) {
    $actionName = DEFAULTACTION;
}
//Si todo esta bien, creamos una instancia del controlador
$controller = new $controllerName();
//Si existen parametros para la acción, llamamos a la accion con parametros,
//sino, solo a la acción
call_user_func(array($controller, "_call"), $actionName, $actionParams);
//if ($actionParams != null) {
//    call_user_func(array($controller, "beforeAction"), array("action" => $actionName, "actionParams" => $actionParams));
//    call_user_func(array($controller, $actionName), $actionParams);
//    call_user_func(array($controller, "afterAction"), array("action" => $actionName, "actionParams" => $actionParams));
//} else {
//    call_user_func(array($controller, "beforeAction"), array("action" => $actionName, "actionParams" => null));
//    call_user_func(array($controller, $actionName));
//    call_user_func(array($controller, "afterAction"), array("action" => $actionName, "actionParams" => null));
//}
?>
 