(function(a){
    a(document).ready(function(b){
        a('.delete').click(function(){
            var answer = confirm('¿Desea realizar esta acción?');
            return answer // answer is a boolean
        });  
        /*datepicker y datetimepicker*/
        a("#datetimepicker").datetimepicker({
            minDate: 0,
            maxDate: 730,
            hour: 0,
            minute:0,
            second:0,
            dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm:00',
            changeMonth: true,
            changeYear: true
        });
        /*datatable*/
        a("table.datatable").dataTable({
            sPaginationType:"full_numbers",
            oLanguage: {
                sProcessing:   "Procesando...",
                sLengthMenu:   "Mostrar _MENU_ registros",
                sZeroRecords:  "No se encontraron resultados",
                sInfo:         "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                sInfoEmpty:    "Mostrando desde 0 hasta 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros en total)",
                sInfoPostFix:  "",
                sSearch:       "Buscar:",
                sUrl:          "",
                oPaginate: {
                    sFirst:    "Primero",
                    sPrevious: "Anterior",
                    sNext:     "Siguiente",
                    sLast:     "Último"
                }
            }
        });
        a("table#eventos-datatable").dataTable({
            sPaginationType:"full_numbers",
            oLanguage: {
                sProcessing:   "Procesando...",
                sLengthMenu:   "Mostrar _MENU_ registros",
                sZeroRecords:  "No se encontraron resultados",
                sInfo:         "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                sInfoEmpty:    "Mostrando desde 0 hasta 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros en total)",
                sInfoPostFix:  "",
                sSearch:       "Buscar:",
                sUrl:          "",
                oPaginate: {
                    sFirst:    "Primero",
                    sPrevious: "Anterior",
                    sNext:     "Siguiente",
                    sLast:     "Último"
                }
            }
        });
        /*spinner*/
        if(a.fn.spinner){
            a(".spinner#evento-invitados").spinner({min: 0, step:1, mouseWheel: true })
            a(".spinner#evento-costo").spinner({min: 0, step:1, mouseWheel: true })            
        }
        
        /*scroll horizontal*/
        if(a.fn.tinyscrollbar){
            a(".da-panel.scrollable .da-panel-content").each(function(){
                var e=a(this).height(),f=a(this).children().wrapAll('<div class="overview"></div>').end().children().wrapAll('<div class="viewport"></div>').end().find(".viewport").css("height",e).end().append('<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>').tinyscrollbar({
                    axis:"x"
                });
                a(window).resize(function(){
                    f.tinyscrollbar_update()
                })
            })
        }
        /*chozen*/
        if(a.fn.autocomplete){            
            if(a.fn.chosen){
                a("select.chozen").chosen();
                a("#habilidades-chozen").chosen();
                a("#servicios-chozen").chosen();
            }            
        }
        /*validación*/
        if(a.fn.validate){
            a("#tarea-add-form").validate({
                rules:{
                    tareaname:{
                        required:true,
                        minlength:6,
                        maxlength:50
                    },
                    tareadesc:{
                        maxlength:200
                    }
                },
                invalidHandler:function(e,c){
                    var f=c.numberOfInvalids();
                    if(f){
                        var d=f==1?"Hay error en 1 campo. Corrigelo":"Hay errores en "+f+" campos. Corrigelos";
                        a("#tarea-add-error").html(d).show()
                    }else{
                        a("#tarea-add-error").hide()
                    }
                }
            });
            a("#rrhh-add-form").validate({
                rules:{
                    username:{
                        required:true,
                        minlength:6,
                        maxlength:50
                    },
                    password:{
                        required:true,
                        minlength:6,
                        maxlength:50                       
                    },
                    password2:{
                        required:true,
                        minlength:6,
                        maxlength:50,
                        equalTo: "#password"
                    },
                    rol:{
                        required:true
                    },
                    mail:{
                        required: true,
                        email:true
                    },
                    habilidades:{
                        required:true
                    },
                    telefono:{
                        digits:true
                    }
                },
                invalidHandler:function(e,c){
                    var f=c.numberOfInvalids();
                    if(f){
                        var d=f==1?"Hay error en 1 campo. Corrigelo":"Hay errores en "+f+" campos. Corrigelos";
                        a("#rrhh-add-error").html(d).show()
                    }else{
                        a("#rrhh-add-error").hide()
                    }
                }
            });
            a("#evento-add-form").validate({
                rules:{
                    fecha:{
                        required:true
                    },
                    lugar:{
                        required:true,
                        minlength:6,
                        maxlength:255                       
                    },
                    cantidad_invitados:{
                        required:true,                        
                        digits: true,
                        min:1,
                        max:50000
                    },
                    costo:{
                        required:true,
                        digits:true,
                        min:1,
                        max:1000000000
                    },
                    mail:{
                        required: true,
                        email:true
                    },
                    habilidades:{
                        required:true
                    },
                    telefono:{
                        digits:true
                    }
                },
                invalidHandler:function(e,c){
                    var f=c.numberOfInvalids();
                    if(f){
                        var d=f==1?"Hay error en 1 campo. Corrigelo":"Hay errores en "+f+" campos. Corrigelos";
                        a("#evento-add-error").html(d).show()
                    }else{
                        a("#evento-add-error").hide()
                    }
                }
            });
            a("#servicio-add-form").validate({
                rules:{
                    nombre:{
                        required:true
                    }
                },
                invalidHandler:function(e,c){
                    var f=c.numberOfInvalids();
                    if(f){
                        var d=f==1?"Hay error en 1 campo. Corrigelo":"Hay errores en "+f+" campos. Corrigelos";
                        a("#servicio-add-error").html(d).show()
                    }else{
                        a("#servicio-add-error").hide()
                    }
                }
            });
        }
    })
})(jQuery);