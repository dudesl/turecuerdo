<?php

namespace Libs;

/**
 *
 * @author Lenovo
 */
interface IController {
    
    public function index($params = null);
    
    public function beforeAction($action, $params = null);
    
    public function afterAction($action, $params = null);
    
    public function _call($action, $actionParams = null);
}

?>
