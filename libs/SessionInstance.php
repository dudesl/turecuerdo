<?php

namespace Libs;

class SessionInstance{
    
    private static $instance = null;
    
    protected $sessionStorage = array();
    
    private function __construct($name, $id){
    
        session_start();
        session_id($id);
        session_name($name);        
        if  (isset($_SESSION['sessionStorage']))
            $this->sessionStorage = $_SESSION['sessionStorage'];        
    }
    
    public static function singleton($name, $id){
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c($name, $id);
        }
 
        return self::$instance;
    }
    
    public function __destruct() {
        $_SESSION['sessionStorage'] = $this->sessionStorage ;
    }
    
    public function getSessionStorage() {
        return $this->sessionStorage;
    }

    public function setSessionStorage($sessionStorage) {
        $this->sessionStorage = $sessionStorage;
    }
    
    public function put($nameObj, $obj){
        $this->sessionStorage[$nameObj] = $obj;
    }
    
    public function get($nameObj){
        return $this->sessionStorage[$nameObj];
    }


    
}
?>
