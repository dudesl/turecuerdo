<?php
namespace Libs;

use Libs\View;
use Libs\IController;

abstract class AbstractController implements IController{
    
    protected $view;
    
    function __construct()
    {
        $this->view = new View();
    }
    
    public function beforeAction($action, $actionParams = null){
        
    }
    
    public function afterAction($action, $actionParams = null){
        
    }
    
    public function _call($action, $actionParams = null){
        $this->beforeAction($action, $actionParams);
        $this->$action($actionParams);
        $this->afterAction($action, $actionParams);
    }
}