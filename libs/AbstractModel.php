<?php
namespace Libs;

class AbstractModel {

    protected $db;    

    public function __construct() {
        $this->db = SPDO::singleton();
    }
    
    /**
     * Ejecuta una sentencia SQL a traves de un PDOStatement 
     * @see PDOStatement()
     * @param string $sql Una cadena sql con los parametros preparados para PDO.  
     *           De la siguiente manera: SELECT * FROM tabla WHERE param1=:param1
     * @param array $data Una correspondencia con los parametros de la consulta SQL. 
     *           Si en la consulta pusimos :param1, entonces $data=array("param1"=>"valordeparam1")
     * @return PDOStatement
     * @throws PDOException 
     */
    protected function executeStatement($sql, array $data = null){
        
        $prepareStat = $this->db->prepare($sql);

        if (!$prepareStat->execute($data)) {
            throw new \PDOException("Error en la consulta ".$sql);
        };
        
        return $prepareStat;
        
    }
    
    
    /**
     * Comienzo de transaccion
     * @return bool 
     */
    protected function beginTransaction(){
        return $this->db->beginTransaction();
    }
    
    /**
     * Ejecuta la pila de sentencias cargada en la transacción
     */
    protected function commitTransaction(){
        return $this->db->commit();
    }
    
    /**
     * Deshace y cierra la transacción y devuelve la bd a autocommit
     */
    protected function rollBackTransaction(){
        return $this->db->rollBack();
    }
    
    /**
     * Agrega una sentencia a ser ejecutada en la transaccion
     */
    protected function execSqlTransaction($sql){
        return $this->db->exec($sql);
    }
    
    /**
     * Muestra la query sql a partir de una query armada para PDO (preparedStatement)
     * @param type $query query preparada para el preparedStatement
     * @param type $params parametros de la query
     * @return string La query
     */
    public function showQuery($query, $params)
    {
        $keys = array();
        $values = array();
        
        // build a regular expression for each parameter
        foreach ($params as $key=>$value)
        {
            if (is_string($key))
            {
                $keys[] = '/:'.$key.'/';
            }
            else
            {
                $keys[] = '/[?]/';
            }
            
            if(is_numeric($value))
            {
                $values[] = intval($value);
            }
            else
            {
                $values[] = '"'.$value .'"';
            }
        }
        
        $query = preg_replace($keys, $values, $query, 1, $count);
        return $query;
    }
}