<?php
namespace Libs;

class View {

    function __construct() {
        
    }
    
    public function show($name, $vars = array()) {
        //$name es el nombre de nuestra plantilla, por ej, listado.php
        //$vars es el contenedor de nuestras variables, es un arreglo del tipo llave => valor, opcional.
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config::singleton();

        //Armamos la ruta a la plantilla
        $path = $config->get('viewsFolder') . $name;

        //Si no existe el fichero en cuestion, tiramos un 404
        if (file_exists($path) == false) {
            trigger_error('Template `' . $path . '` does not exist.', E_USER_NOTICE);
            return false;
        }

        //Si hay variables para asignar, las pasamos una a una.
        if (is_array($vars)) {
            foreach ($vars as $key => $value) {
                $$key = $value;
            }
        }
        
        
        //include sidebar
        include_once($config->get('viewsFolder') . 'sidebar.php');
        //Finalmente, incluimos las plantillas.
        //incluimos primero el encabezado
        if (array_key_exists('no_head', $vars) == false){
                include($config->get('viewDefaultHead'));
        }
        //Luego incluimos la vista del controlador
        include($path);
        //Luego incluimos el footer comun a todas las plantillas
        if (array_key_exists('no_foot', $vars) == false){
                include($config->get('viewDefaultFoot'));            
        }
    }
    
    public function showWhitoutHeadFoot($name, $vars = array()) {
        //$name es el nombre de nuestra plantilla, por ej, listado.php
        //$vars es el contenedor de nuestras variables, es un arreglo del tipo llave => valor, opcional.
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config::singleton();

        //Armamos la ruta a la plantilla
        $path = $config->get('viewsFolder') . $name;

        //Si no existe el fichero en cuestion, tiramos un 404
        if (file_exists($path) == false) {
            trigger_error('Template `' . $path . '` does not exist.', E_USER_NOTICE);
            return false;
        }

        //Si hay variables para asignar, las pasamos una a una.
        if (is_array($vars)) {
            foreach ($vars as $key => $value) {
                $$key = $value;
            }
        }

        //Finalmente, incluimos la plantillas.        
        //Luego incluimos la vista del controlador
        include($path);        
    }

}

/*
 El uso es bastante sencillo:
 $vista = new View();
 $vista->show('listado.php', array("nombre" => "Juan"));
*/