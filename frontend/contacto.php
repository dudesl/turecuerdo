<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
        <title>Tu Recuerdo - Contacto</title>
        <meta name="description" content="" />
        <link rel="shortcut icon" href="http://themes.two2twelve.com/site/limon/restaurant/images/favicon.png" />	
        <link rel="stylesheet" type="text/css" href="stylesheets/nivo-slider.css" />		
        <link rel="stylesheet" type="text/css" href="javascripts/fancybox/jquery.fancybox-1.3.4.css" />
        <link rel="stylesheet" type="text/css" href="stylesheets/limon.css" />
        <link rel="stylesheet" type="text/css" href="stylesheets/colors/lime.css" media="screen" />
        <script type="text/javascript" src="javascripts/jquery-1.6.2.js"></script>
        <script type="text/javascript" src="javascripts/jquery-ui-1.8.14.js"></script>
        <script type="text/javascript" src="javascripts/jquery.nivo.slider.js"></script>
        <script type="text/javascript" src="javascripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <script type="text/javascript" src="javascripts/jquery.validate.min.js"></script>
        <script type="text/javascript" src="javascripts/global.js"></script>

        <link rel="stylesheet" type="text/css" href="stylesheets/colorpicker.css" />
        <script type="text/javascript" src="javascripts/colorpicker.js"></script>        
        <script type="text/javascript" src="javascripts/demo.js"></script>
        
    </head>
    <body>
        <!-- Start Page Container -->
        <div id="container_wrap">

            <!-- Start Header -->
            <div id="header">			
                <!-- Start Logo -->
                <div class="logo">
                    <a href="index.html"><img src="images/header-title1.png" alt="Tu Recuerdo Eventos" /></a>
                </div>
                <!-- End Logo -->

                <!-- Start Navigation -->
                <ul id="nav">
                    <li><a href="index.html">Inicio</a></li>
                    <li><a href="laempresa.html">Nuestra historia</a></li>
                    <li><a href="servicios.html">Servicios</a></li>
                    <li><a href="galeria.html">Galeria</a></li>
                    <li class="active"><a href="contacto.php">Contacto</a></li>
                </ul>	
                <!-- End Navigation -->

            </div>
            <!-- End Header -->


            <!-- Start Page Content -->
            <div id="contact_us" class="main_content">

                <div class="page_heading">
                    <h1>Contactes&eacute; con nosotros</h1>	
                </div>

                <div class="small_left_column">
                    <?php if (isset($_GET['success'])): ?>
                        <div class="box success">
                            <p>Su mensaje ha sido enviado con exito</p>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($_GET['error'])): ?>
                        <div class="box error">
                            <p>Su mensaje no ha podido enviarse. Intenteló nuevamente</p>
                        </div>
                    <?php endif; ?>

                    <div class="box error" id="contact-error" style="display:none">
                        <p>Su mensaje no ha podido enviarse. Intenteló nuevamente</p>
                    </div>

                    <form action="send.php" method="post" id="contact_form">
                        <div class="row">
                            <p>
                                <label>Su Nombre</label>
                                <input type="text" name="nombre" class="text_field" />
                            </p>
                            <p>
                                <label>Su Correo</label>
                                <input type="text" name="email" class="text_field" />
                            </p>
                        </div>
                        <div class="row">
                            <p>
                                <label>Su telefono</label>
                                <input type="text" name="telefono" class="text_field" />
                            </p>
                            <p>
                                <label>Asunto</label>
                                <input type="text" name="asunto" class="text_field" />
                            </p>
                        </div>
                        <p>
                            <label>Consulta</label>
                            <textarea class="text_field" rows="10" cols="10" name="mensaje"></textarea>
                        </p>
                        <input type="submit" class="button" value="Enviar Mensaje" />
                    </form>

                </div><!-- end left column-->

                <div id="sidebar" class="large_right_column">

                    <div id="hq" class="container">
                        <h3>Oficina</h3>
                        <div class="content">
                            <ul>
                                <li class="mail">
                                    <b>Correo electr&oacute;nico</b>
                                    <p>
                                        info@turecuerdo.com
                                    </p>						
                                </li>
                                <li class="phone">
                                    <b>Telefono</b>
                                    <p>xxx-xxx-xxxx</p>								
                                </li>
                                <li class="fax last">
                                    <b>Fax</b>
                                    <p>xxx-xxx-xxxx</p>					
                                </li>
                            </ul>
                        </div>
                    </div>

                </div><!-- end right column-->

            </div>
            <!-- End Page Content -->

            <!-- Start Footer -->
            <div id="footer">
                <div class="left">
                    <p>Copyright &copy; Tu Recuerdo 2012. Todos los derechos reservados.</p>
                </div>

                <div class="right">
                    <ul>
                        <li class="facebook"><a href="index.html">Facebook</a></li>
                        <li class="twitter"><a href="index.html">Twitter</a></li>
                        <li class="flickr"><a href="index.html">Facebook</a></li>
                    </ul>
                </div>
            </div>
            <!-- End Footer -->

        </div>
        <!-- End Page Container -->
        
        <script>
                $("#contact_form").validate({
                    rules:{
                        nombre:{
                            required:true,
                            minlength:6,
                            maxlength:100
                        },
                        email:{
                            required:true,
                            email:true,
                            maxlength:100
                        },
                        telefono:{
                            digits: true
                        },
                        asunto:{
                            required:true,
                            maxlength:100
                        },
                        mensaje:{
                            required:true,
                            maxlength:500
                        }
                        
                    },
                    invalidHandler:function(e,c){
                        var f=c.numberOfInvalids();
                        if(f){
                            var d=f==1?"Hay error en 1 campo. Corrigelo":"Hay errores en "+f+" campos. Corrigelos";
                            $("#contact-error").html(d).show()
                        }else{
                            $("#contact-error").hide()
                        }
                    }
                });
            
            
        </script>
    </body>			
</html>