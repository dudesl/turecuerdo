SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `turecuerdo` ;
CREATE SCHEMA IF NOT EXISTS `turecuerdo` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci ;
USE `turecuerdo` ;

-- -----------------------------------------------------
-- Table `turecuerdo`.`usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `turecuerdo`.`usuario` ;

CREATE  TABLE IF NOT EXISTS `turecuerdo`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificacion del usuario' ,
  `username` VARCHAR(100) NOT NULL COMMENT 'nombre de usuario' ,
  `password` VARCHAR(100) NOT NULL COMMENT 'contraseña' ,
  `nombre` VARCHAR(100) NOT NULL COMMENT 'nombre del empleado' ,
  `direccion` VARCHAR(250) NULL COMMENT 'direccion del empleado (opcional)' ,
  `telefono` INT NOT NULL COMMENT 'telefono del empleado ' ,
  `mail` VARCHAR(100) NULL COMMENT 'mail del telefono' ,
  `rol` VARCHAR(100) NOT NULL COMMENT 'identificador del rol en el sistema del usuario' ,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish2_ci
COMMENT = 'tabla de usuarios'
PACK_KEYS = DEFAULT;

CREATE UNIQUE INDEX `username_UNIQUE` ON `turecuerdo`.`usuario` (`username` ASC) ;

-- -----------------------------------------------------
-- Table `turecuerdo`.`evento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `turecuerdo`.`evento` ;

CREATE  TABLE IF NOT EXISTS `turecuerdo`.`evento` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `lugar` VARCHAR(250) NOT NULL ,
  `fecha` DATETIME NOT NULL ,
  `cantidad_invitados` SMALLINT NOT NULL ,
  `costo` DOUBLE NOT NULL ,
  `descripción` VARCHAR(250) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `turecuerdo`.`evento_has_empleado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `turecuerdo`.`evento_has_empleado` ;

CREATE  TABLE IF NOT EXISTS `turecuerdo`.`evento_has_empleado` (
  `evento_id` INT NOT NULL ,
  `empleado_usuarios_id` INT NOT NULL ,
  PRIMARY KEY (`evento_id`, `empleado_usuarios_id`) ,
  CONSTRAINT `fk_evento_has_empleado_evento`
    FOREIGN KEY (`evento_id` )
    REFERENCES `turecuerdo`.`evento` (`id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_evento_has_empleado_empleado`
    FOREIGN KEY (`empleado_usuarios_id` )
    REFERENCES `turecuerdo`.`empleado` (`usuarios_id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `turecuerdo`.`tarea`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `turecuerdo`.`tarea` ;

CREATE  TABLE IF NOT EXISTS `turecuerdo`.`tarea` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'identificador de la tarea' ,
  `nombre` VARCHAR(100) NOT NULL COMMENT 'nombre de la tarea' ,
  `descripcion` VARCHAR(250) NULL COMMENT 'descripción de la tarea (opcional)' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `turecuerdo`.`servicio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `turecuerdo`.`servicio` ;

CREATE  TABLE IF NOT EXISTS `turecuerdo`.`servicio` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL ,
  `descripcion` VARCHAR(250) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `turecuerdo`.`empleado_has_tarea`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `turecuerdo`.`tarea_asignada` ;

CREATE  TABLE IF NOT EXISTS `turecuerdo`.`tarea_asignada` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `empleado_id` INT NOT NULL ,
  `tarea_id` INT NOT NULL ,
  `evento_id` INT NOT NULL,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `fk_id_empleado_asignado`
    FOREIGN KEY (`empleado_id` )
    REFERENCES `turecuerdo`.`empleado` (`usuarios_id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_id_tarea_asignada`
    FOREIGN KEY (`tarea_id` )
    REFERENCES `turecuerdo`.`tarea` (`id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_id_evento_asignado`
    FOREIGN KEY (`tarea_id` )
    REFERENCES `turecuerdo`.`tarea` (`id` )
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;