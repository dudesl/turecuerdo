<?php
/**
 * El Bootstrap se encarga de hacer el load de todas las librerias externas.
 * Tambien se encarga de realizar la conexión a la base de datos 
 */

$app = 'app';
$libs = 'libs';
/**
 * TOMADO DE KOHANA FRAMEWORK:
 * Set the PHP error reporting level. If you set this in php.ini, you remove this.
 * @see  http://php.net/error_reporting
 *
 * When developing your application, it is highly recommended to enable notices
 * and strict warnings. Enable them by using: E_ALL | E_STRICT
 *
 * In a production environment, it is safe to ignore notices and strict warnings.
 * Disable them by using: E_ALL ^ E_NOTICE
 *
 * When using a legacy application with PHP >= 5.3, it is recommended to disable
 * deprecated notices. Disable with: E_ALL & ~E_DEPRECATED
 */
error_reporting(E_ALL | E_STRICT); //Error reporting level



// Hace la aplicación relativa al docroot
if ( ! is_dir($app) && is_dir(DOCROOT.$app)){
        $app = DOCROOT.$app;}

// Hace las librerias relativas al docroot
if ( !is_dir($libs) && is_dir(DOCROOT.$libs)){
        $libs = DOCROOT.$libs;}

// Define the absolute paths for configured directories
define('APPPATH', realpath($app));
define('LIBPATH', realpath($libs));

// Clean up the configuration vars
unset($app, $libs);

//Define el path absoluto a las carpetas mvc
define('CONTROLLERSPATH', APPPATH.DIRECTORY_SEPARATOR.'controllers/');
define('MODELSPATH', APPPATH.DIRECTORY_SEPARATOR.'models/');
define('VIEWSPATH', APPPATH.DIRECTORY_SEPARATOR.'views/');
define('ENTITIESPATH', APPPATH.DIRECTORY_SEPARATOR.'entities/');

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('America/Argentina/San_Luis');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'es_AR.utf-8');

/**
 * Autoload de librerias - Class loader 
 * 
 * @see http://gitnacho.github.com/symfony-docs-es/components/class_loader.html
 * @see https://github.com/symfony/ClassLoader
 */
include_once('app/loader.php');

/**
 * Autoload de librerias - Class loader 
 * 
 * @see http://gitnacho.github.com/symfony-docs-es/components/class_loader.html
 * @see https://github.com/symfony/ClassLoader
 */
include_once 'app/config.php';

?>
