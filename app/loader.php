<?php
//require del ClassLoader de Symfony
require_once LIBPATH.'/ClassLoader.php'; 

use Libs\ClassLoader;

$loader = new ClassLoader();

//sete un include path para el ClassLoader
$loader->setUseIncludePath(array(get_include_path(),DOCROOT));
$loader->addPrefix(null , LIBPATH);
$loader->addPrefix(null , APPPATH);
$loader->addPrefix(null , CONTROLLERSPATH);
$loader->addPrefix(null , MODELSPATH);
$loader->addPrefix(null , ENTITIESPATH);
    
/**
 * registra las clases y ejecuta el apunte a la función de auto_load
 * Lo que hace el register es utilizar la función spl_autoload_register() que 
 * registra, en una cola especializada, una función para buscar las clases no 
 * encontradas al momento de hacer un new
 * 
 * @see http://php.net/manual/es/function.spl-autoload-register.php
 */

$loader->register();

?>
