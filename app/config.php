<?php 
//Instancia de Config. Parametros de configuración de la aplicación
use Libs\Config;
$config = Config::singleton();

//Ficheros de controladores, modelos y vistas
$config->set('controllersFolder', CONTROLLERSPATH);
$config->set('modelsFolder', MODELSPATH);
$config->set('viewsFolder', VIEWSPATH);

//Direccion del encabezado y el pie de las vistas
$config->set('viewDefaultHead', $config->get('viewsFolder').'defaultheader.php');
$config->set('viewDefaultFoot', $config->get('viewsFolder').'defaultfooter.php');

//Datos de la base de datos
$config->set('dbhost', 'localhost');
$config->set('dbname', 'turecuerdo');
$config->set('dbuser', 'dudesl');
$config->set('dbpass', 'dudesl');
?>