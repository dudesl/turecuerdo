<?php

namespace Model;

use Libs\AbstractModel;
use Model\TareaModel;

class ServicioModel extends AbstractModel {

    const TABLE = 'servicio';
    const TABLE2 = 'servicio_has_tarea';

    /**
     * Obtiene un servicio a partir de su identificador
     * @param type $servicioid 
     */
    public function get($servicioid) {
        $sql = "SELECT * FROM " . self::TABLE . " WHERE id=:id";

        try {
            $servicioStatement = $this->executeStatement($sql, array('id' => $servicioid));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        $servicio = $servicioStatement->fetch(\PDO::FETCH_OBJ);
        return $servicio;
    }

    public function getFromNombre($servicioNombre) {
        $sql = "SELECT * FROM " . self::TABLE . " WHERE nombre=:nombre";

        try {
            $servicioStatement = $this->executeStatement($sql, array('nombre' => $servicioNombre));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        $servicio = $servicioStatement->fetch(\PDO::FETCH_OBJ);
        return $servicio;
    }

    public function getFromEvento($eventoid) {
        $sql = "SELECT servicio.id, servicio.nombre, servicio.descripcion";
        $sql.=" FROM " . self::TABLE;
        $sql.=" INNER JOIN evento_has_servicio";
        $sql.=" ON evento_has_servicio.servicio_id= servicio.id ";
        $sql.=" AND evento_has_servicio.evento_id = :evento_id";
        
        try {
            $serviciosStatement = $this->executeStatement($sql, array('evento_id' => $eventoid));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        return $servicios = $serviciosStatement->fetchAll(\PDO::FETCH_CLASS, 'Servicio');
    }

    /**
     * Obtiene todos los servicios cargados hasta el momento 
     */
    public function getAll() {
        $sql = "SELECT * FROM " . self::TABLE;

        try {
            $servicioStatement = $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        $servicios = $servicioStatement->fetchAll(\PDO::FETCH_CLASS, "Servicio");
        return $servicios;
    }

    /**
     * Agrega un servicio a la base de datos
     * @param type $servicio 
     */
    public function add($servicio, $tareas) {
        $sql = "INSERT INTO " . self::TABLE . " (nombre,descripcion)";
        $sql.=" VALUES (:nombre, :descripcion)";
        
        try {
            $this->executeStatement($sql, $servicio);
        } catch (\PDOException $e) {
            throw new DBException("", 2);
        }

        $servicioid = $this->getByNombre($servicio['nombre'])->id;
        $tareas != null ? $this->addAsignacionTareas($servicioid, $tareas) : false;
        
        return "Agregado nuevo servicio";
    }

    /**
     * Elimina un servicio a partir de su identificador
     * @param type $servicioid 
     */
    public function delete($servicioid) {
        $sql = "DELETE FROM " . self::TABLE . " WHERE id=:servicioid";
        try {
            $this->executeStatement($sql, array('servicioid' => $servicioid));
        } catch (\PDOException $e) {
            throw new DBException("", 3);
        }
    }

    /**
     * Actualiza un servicio con los datos pasados. El parametro $tareas, si no
     * es nulo, significa que se actualizan las tareas relacionadas con el servicio
     * @param array Arreglo con los datos del servicio
     * @param array Arreglo con las tareas asignadas al servicio 
     */
    public function update($servicio, $tareas = null) {
        $sql = "UPDATE " . self::TABLE . " SET nombre=:nombre, descripcion=:descripcion WHERE id=:id";

        try {
            $this->executeStatement($sql, $servicio);            
        } catch (\PDOException $e) {
            throw new DBException("", 4);
        }
        
        $tareas != null ? $this->updateAsignacionTarea($servicio['id'], $tareas) : false;
            
    }

    public function getByNombre($nombre) {
        $sql = "SELECT * FROM " . self::TABLE . " WHERE nombre=:nombre";
        try {
            $servicioStatement = $this->executeStatement($sql,array('nombre'=>$nombre));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }
        
        return $servicio = $servicioStatement->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * Obtiene las tareas relacionadas con el servicio
     * @param type $servicioid 
     */
    public function getTareasByServicio($servicioid) {
        $sql = "SELECT tarea.id, tarea.nombre, tarea.descripcion" .
               " FROM tarea" .
               " INNER JOIN servicio_has_tarea" .
               " ON servicio_has_tarea.id_tarea= tarea.id" .
               " AND servicio_has_tarea.id_servicio = :servicio_id";

        try {
            $tareasStatement = $this->executeStatement($sql, array('servicio_id' => $servicioid));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        return $tareas = $tareasStatement->fetchAll(\PDO::FETCH_CLASS, 'Tarea');
    }

    /**
     * Actualiza las tareas relacionadas con el servicio
     * @param int Identificador del servicio
     * @param array Arreglo con las tareas asignadas al servicio
     */
     public function updateAsignacionTarea($servicioid, $tareas) {
        $this->deleteAsignacionTarea($servicioid);
        $this->addAsignacionTareas($servicioid, $tareas);
    }

    /**
     * Elimina las tareas relacionadas con el servicio
     * @param int Identificador del servicio
     */
     public function deleteAsignacionTarea($servicioid) {
        $sql = "DELETE FROM ".self::TABLE2." WHERE id_servicio=:servicio_id";
        try {
            $this->executeStatement($sql, array('servicio_id' => $servicioid));
        } catch (\PDOException $exc) {
            throw new DBException("", 3);
        }
    }

    /**
     * Agrega tareas relacionadas con el servicio
     * @param int Identificador del servicio
     * @param array Arreglo con las tareas asignadas al servicio
     */
    public function addAsignacionTareas($servicioid, $tareas) {
        foreach ($tareas as $tarea) {
            $this->addAsignacionTarea($servicioid, $tarea);
        }
    }
    
    public function addAsignacionTarea($servicioid, $tarea){
        $sql .= "INSERT INTO " . self::TABLE2 . " (id_servicio, id_tarea)";
        $sql .=" VALUES (:servicio_id, :tarea); ";
        
            try {
                $this->executeStatement($sql, array("servicio_id"=>$servicioid, "tarea"=>$tarea));
            } catch (\PDOException $e) {
                throw new DBException("", 2);
            }
        
    }
}

?>
