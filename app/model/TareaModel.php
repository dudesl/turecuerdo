<?php

namespace Model;

use Libs\AbstractModel;

/**
 * Description of TareaModel
 *
 * @author dudesl
 */
class TareaModel extends AbstractModel {

    const TABLE = 'tarea';

    function __construct() {
        parent::__construct();
    }

    public function add($tareaname, $tareadesc = null) {
        $sql = "INSERT INTO " . self::TABLE . " (nombre,descripcion) VALUES (:name,:desc)";
        
        try {
            $this->executeStatement($sql, array('name' => $tareaname, 'desc' => $tareadesc));
        } catch (\PDOException $e) {
            throw new DBException("", 2);
        }

        
    }
    
    public function delete($tareaid){
        $sql = "DELETE FROM " . self::TABLE . " WHERE id=:id";
        
        try {
            $this->executeStatement($sql, array('id' => $tareaid));
        } catch (\PDOException $e) {
            throw new DBException("", 3);
        }
    }
    
    public function update($tareaid, $tareaname, $tareadesc = null){
        $sql = "UPDATE " . self::TABLE . " SET nombre=:name,descripcion=:desc WHERE id=:id";
        
        try {
            $this->executeStatement($sql, array('id'=>$tareaid,'name' => $tareaname, 'desc' => $tareadesc));
        } catch (\PDOException $e) {
            throw new DBException("", 4);
        }
    }
    
    public function getAll() {        
        $sql = "SELECT * FROM ". self::TABLE;
        try {
            $prepareStat = $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }
        
        return $tareas = $prepareStat->fetchAll(\PDO::FETCH_CLASS, "Tarea");        
    }
    
    public function get($tareaid) {        
        $sql = "SELECT * FROM ". self::TABLE . " WHERE id = :id";

        try {
            $prepareStat = $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 4);
        }
        return $tarea = $prepareStat->fetch(\PDO::FETCH_OBJ);        
    }

}

?>
