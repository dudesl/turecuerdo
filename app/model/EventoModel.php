<?php

namespace Model;

use Libs\AbstractModel;
use Model\ServicioModel;
use Model\UserModel;
use Model\TareaModel;

class EventoModel extends AbstractModel {
    //Tablas sobre las que se trabaja en el modelo

    const TABLE = "evento";
    const TABLE2 = "asignacion_tarea";
    const TABLE3 = "evento_has_servicio";

    /**
     * Agrega un nuevo evento
     * @param array $evento Datos del evento
     * @param array $servicios Arreglo de servicios dentro del evento     
     */
    public function add($evento, $servicios) {
        $sql = "INSERT INTO " . self::TABLE . " (lugar, fecha, cantidad_invitados, costo, descripcion)";
        $sql.=" VALUES (:lugar, :fecha, :cantidad_invitados, :costo, :descripcion)";

        try {
            $this->executeStatement($sql, $evento);
        } catch (\PDOException $e) {
            throw new DBException("", 2);
        }
        $eventoid = $this->getByLugarFecha($evento['lugar'], $evento['fecha'])->id;
        $servicios != null ? $this->addAsignacionServicios($eventoid, $servicios) : false;

        return "Agregado nuevo evento";
    }

    /**
     * Eliminar un evento
     * @param type $eventoid
     * @throws \PDOException 
     */
    public function delete($eventoid) {
        $sql = "DELETE FROM " . self::TABLE . " WHERE id=:id";

        try {
            $this->executeStatement($sql, array('id' => $eventoid));
        } catch (\PDOException $exc) {
            throw new DBException("", 3);
        }
    }

    /**
     * Actualizar un evento
     * @param type $evento
     * @param type $servicios
     * @throws \PDOException 
     */
    public function update($evento, $servicios = null) {
        $sql = "UPDATE " . self::TABLE . " SET lugar=:lugar, fecha=:fecha, cantidad_invitados=:cantidad_invitados, costo=:costo, descripcion=:descripcion WHERE id=:id";

        try {
            $this->executeStatement($sql, $evento);
        } catch (\PDOException $e) {
            throw new DBException("", 4);
        }
        $eventoid = $this->getByLugarFecha($evento['lugar'], $evento['fecha'])->id;
        $servicios != null ? $this->updateAsignacionServicios($eventoid, $servicios) : false;
    }

    /**
     * Obtener un evento
     * @param type $eventoid
     * @return type
     * @throws \PDOException Obtener
     */
    public function get($eventoid) {
        $sql = "SELECT * FROM " . self::TABLE . " WHERE id=:id";

        try {
            $eventoStatement = $this->executeStatement($sql, array('id' => $eventoid));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        return $evento = $eventoStatement->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * Obtener todos los eventos
     * @return type
     * @throws \PDOException 
     */
    public function getAll() {
        $sql = "SELECT * FROM " . self::TABLE;

        try {
            $eventosStatement = $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        return $eventos = $eventosStatement->fetchAll(\PDO::FETCH_CLASS, 'Evento');
    }

    /**
     * Obtener evento por lugar y fecha
     * @param type $lugar
     * @param type $fecha
     * @return type Ob
     */
    public function getByLugarFecha($lugar, $fecha) {
        $sql = "SELECT id FROM " . self::TABLE . " WHERE lugar=:lugar AND fecha=:fecha";
        
        try {
            $idStatement = $this->executeStatement($sql, array('lugar' => $lugar, 'fecha' => $fecha));
        } catch (\PDOException $exc) {
            throw new DBException("", 1);
        }

        return $this->get($idStatement->fetch(\PDO::FETCH_OBJ)->id);
    }

    /**
     * Agregar varias asignaciones de servicios a un evento
     * @param type $eventoid
     * @param type $servicios
     * @throws \PDOException 
     */
    public function addAsignacionServicios($eventoid, $servicios) {
        $sql = "";
        foreach ($servicios as $servicio) {
            $this->addAsignacionServicio($eventoid, $servicio);
        }
    }

    /**
     * Agregar una asignacion de servicio a un evento
     * @param type $eventoid
     * @param type $servicioid
     * @throws \PDOException 
     */
    public function addAsignacionServicio($eventoid, $servicioid) {
        $sql = "INSERT INTO " . self::TABLE3 . " (evento_id, servicio_id)";
        $sql .=" VALUES (:evento_id, :servicio_id); ";
        try {
            $this->executeStatement($sql, array("evento_id"=>$eventoid, "servicio_id"=>$servicioid));
        } catch (\PDOException $e) {
            throw new DBException("", 2);
        }
    }
    
    /**
     * Elimina las asignaciones de servicios de un evento
     * @param type $eventoid 
     */
    public function deleteAsignacionServicios($eventoid) {
        $sql = "DELETE FROM evento_has_servicio WHERE evento_id=:evento_id";
        try {
            $this->executeStatement($sql, array('evento_id' => $eventoid));
        } catch (\PDOException $e) {
            throw new DBException("", 3);            
        }
    }

    /**
     * Actualiza las asignaciones de servicios de un evento
     * @param type $eventoid
     * @param type $servicios 
     */
    public function updateAsignacionServicios($eventoid, $servicios) {
        $this->deleteAsignacionServicios($eventoid);
        $this->addAsignacionServicios($eventoid, $servicios);
    }

    /**
     * Obtengo las asignaciones de un evento, filtradas solo por el evento,
     *  por evento y tarea, por evento y empleado, o por las 3
     * @param type $eventoid
     * @param type $tarea
     * @param type $empleadoid
     * @return type
     * @throws \PDOException 
     */
    public function getAsignacion($eventoid, $tarea = null, $empleadoid = null) {
        $sql = "SELECT * FROM " . self::TABLE2 . " WHERE evento_id=:evento";
        $data['evento'] = $eventoid;
        if (isset($servicioid)) {
            $sql.=" AND tarea_id=:servicio";
            $data['servicio'] = $servicioid;
        }

        if (isset($empleadoid)) {
            $sql.=" AND empleado_id=:empleado";
            $data['empleado'] = $empleadoid;
        }

        try {
            $asignacionesStatement = $this->executeStatement($sql, $data);
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        return $asignaciones = $asignacionesStatement->fetchAll(\PDO::FETCH_CLASS, 'Asignacion');
    }

    public function getAsignacionByUsuario($usuarioid) {
        $sql = "SELECT * FROM " . self::TABLE2 . " WHERE usuario_id=:usuario_id GROUP BY evento_id";

        try {
            $asignacionesStatement = $this->executeStatement($sql, array("usuario_id" => $usuarioid));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        return $asignaciones = $asignacionesStatement->fetchAll(\PDO::FETCH_CLASS, 'Asignacion');
    }

    /**
     * Agrego varias asignaciones de tareas
     *  el parametro de entrada deber ser un arreglo de arreglos de esta forma:
     *  array(
     *      "evento_id" => id del evento,
     *      "usuario_id" => id del empleado,
     *      "tarea_id" => id de la tarea
     */
    public function addAsignacionesTarea($asignaciones, $eventoid){
        foreach ($asignaciones as $asignacion) {
            $this->addAsignacionTarea($asignacion, $eventoid);
        }
    }

    /**
     * Agrega una asignacion de tarea
     *  el parametro de entrada deber ser un arreglo de esta forma:
     *  array(
     *      "usuario_id" => id del empleado,
     *      "tarea_id" => id de la tarea
     * 
     * @param type $asignacion
     * @param type $eventoid
     * @return boolean
     * @throws \PDOException Agre
     */
    public function addAsignacionTarea($asignacion, $eventoid) {
        if ($asignacion == null)
            return false;

        $sql = "INSERT INTO " . self::TABLE2 . " (evento_id, usuario_id, tarea_id)";
        $sql.= " VALUES (:evento_id, :usuario_id, :tarea_id)";

        $data = array(
            "evento_id" => $eventoid,
            "usuario_id" => $asignacion['usuario_id'],
            "tarea_id" => $asignacion['tarea_id']
        );

        try {
            $this->executeStatement($sql, $data);
        } catch (\PDOException $e) {
            throw new DBException("", 2);
        }
    }
    
    /**
     * Elimina las asignaciones de tareas/empleados para determinado evento
     * @param type $eventoid
     * @throws \PDOException 
     */
    public function deleteAsignacionTareaByEvento($eventoid) {
        $sql = "DELETE FROM " . self::TABLE2 . " WHERE evento_id=:evento_id";

        try {
            $this->executeStatement($sql, array("evento_id" => $eventoid));
        } catch (\PDOException $e) {
            throw new DBException("", 3);
        }
    }
    
    /**
     * Actualiza las asignaciones de tareas/empleados para determinado evento
     * @param array $asignaciones
     * @param type $eventoid
     * @return boolean 
     */
    public function updateAsignacionTarea(array $asignaciones, $eventoid) {
        if ($asignaciones == null)
            return false;
        $this->deleteAsignacionTareaByEvento($eventoid);        
        $this->addAsignacionesTarea($asignaciones, $eventoid);        
    }
    
    /**
     * Obtengo todas las tareas a realizar para determinado evento
     * @param type $eventoid
     * @return type 
     */
    public function getTareasByEvento($eventoid) {
        /**
         *  The power of sql :)
         *  SELECT tarea.id, tarea.nombre, tarea.descripcion
            FROM tarea
            WHERE tarea.id IN (
                SELECT servicio_has_tarea.id_tarea
                FROM servicio_has_tarea
                WHERE servicio_has_tarea.id_servicio IN (
                    SELECT evento_has_servicio.servicio_id
                    FROM evento_has_servicio
                    WHERE evento_has_servicio.evento_id = :evento_id
                )
            )
         */
        $sql = "SELECT tarea.id, tarea.nombre, tarea.descripcion";
        $sql.=" FROM tarea ";
        $sql.=" WHERE tarea.id IN ( ";
        $sql.= "SELECT servicio_has_tarea.id_tarea";
        $sql.= " FROM servicio_has_tarea";
        $sql.= " WHERE servicio_has_tarea.id_servicio IN( ";
        $sql.= "SELECT evento_has_servicio.servicio_id";
        $sql.= " FROM evento_has_servicio ";
        $sql.= " WHERE evento_has_servicio.evento_id=:evento_id";
        $sql.= " )";
        $sql.=" )";

        try {
            $tareasStatement = $this->executeStatement($sql, array("evento_id" => $eventoid));
        } catch (\PDOException $e) {
            throw new DBException("", 4);
        }

        return $tareas = $tareasStatement->fetchAll(\PDO::FETCH_CLASS, 'Tarea');
    }
    
    /**
     * Obtiene las tareas asignadas a determinado evento y usuario
     * @param type $eventoid
     * @param type $usuarioid
     * @return type
     * @throws DBException 
     */
    public function getTareasByEventoAndUsuario($eventoid, $usuarioid){
        $sql = "SELECT * FROM tarea
                where tarea.id IN(
                    SELECT asignacion_tarea.tarea_id from asignacion_tarea
                    where usuario_id = :usuario_id and evento_id = :evento_id
                )";
        
        try {
            $tareasStatement = $this->executeStatement($sql, array("evento_id"=>$eventoid, "usuario_id"=>$usuarioid));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }
        
        return $tareasStatement->fetchAll(\PDO::FETCH_CLASS, "Tarea");
    }
    
    /**
     * Obtiene las asignaciones de determinado empleado, con eventos y tareas por evento detalladas
     * @param type $empleadoid
     * @return type 
     */
    public function getFullAsignacion($empleadoid){
        
        $asignaciones = $this->getAsignacionByUsuario($empleadoid);
        $eventosAsignado = null;
        foreach ($asignaciones as $asignacion) {
            $eventoAsignado = $this->get($asignacion->evento_id); 
            $eventoAsignado->tareas = $this->getTareasByEventoAndUsuario($asignacion->evento_id, $asignacion->usuario_id);            
            $eventosAsignado[] = $eventoAsignado;
        }        
        return $eventosAsignado;
    }
}
?>
