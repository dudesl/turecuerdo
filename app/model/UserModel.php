<?php

namespace Model;

use Libs\AbstractModel;
use Libs\DBException;

class UserModel extends AbstractModel {

    const TABLE = 'usuario';

    public function __construct() {
        parent::__construct();
    }

    public function getAll() {
        $sql = "SELECT * FROM " . self::TABLE;

        try {
            $prepareStat = $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        $users = $prepareStat->fetchAll(\PDO::FETCH_CLASS, "User");
        return $users;
    }

    public function get($userid) {
        $sql = "SELECT * FROM " . self::TABLE . " WHERE id=:id";

        try {
            $prepareStat = $this->executeStatement($sql, array('id' => $userid));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        $users = $prepareStat->fetch(\PDO::FETCH_OBJ);
        return $users;
    }

    public function getEmpleados() {
        $sql = "SELECT * FROM " . self::TABLE . " WHERE rol='Empleado'";

        try {
            $prepareStat = $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        $users = $prepareStat->fetchAll(\PDO::FETCH_CLASS, "User");
        return $users;
    }

    public function getByNick($nick) {
//        $prepareStat = $this->db->prepare($sql);
//        $prepareStat->execute();

        $sql = "SELECT * FROM " . self::TABLE . " WHERE username=:username";

        try {
            $prepareStat = $this->executeStatement($sql, array('username' => $nick));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        $user = $prepareStat->fetch(\PDO::FETCH_OBJ);
        return $user;
    }

    public function add($user) {

        $user['password'] = md5($user['password']);
        $habilidades = array_pop($user);


        //insert de usuario
        $sql = "INSERT INTO " . self::TABLE .
                " (username, password, nombre, direccion, telefono, mail, rol)" .
                " VALUES (:username,:password,:nombre,:direccion,:telefono,:mail,:rol)";

        try {
            $userStatement = $this->executeStatement($sql, $user);
            if ($userStatement && $habilidades != '') {
                $userid = $this->getByNick($user['username'])->id;
                try {
                    $this->insertHabilidades($userid, $habilidades);
                } catch (\PDOException $e) {
                    $this->delete($userid);
                    throw new DBException("", 2);
                }
            }
        } catch (\PDOException $e) {
            throw new DBException("", 2);
        }

//        $prepareStat = $this->db->prepare($sql);
//        
//        if (!$prepareStat->execute($user)) {
//            throw new \PDOException("No se pudo insertar el usuario");
//        };
    }

    public function update($user, $habilidades = null) {
        $sql = "UPDATE " . self::TABLE .
                " SET username=:username, nombre=:nombre," .
                " direccion=:direccion, telefono=:telefono, mail=:mail, rol=:rol" .
                " WHERE id=:id";
        try {
            $this->executeStatement($sql, $user);
        } catch (\PDOException $e) {
            throw new DBException("", 4);
        }

        $habilidades != null ? $this->updateHabilidades($user['id'], $habilidades) : false;
    }

    public function updatePass($newPass, $userid) {
        $sql = "UPDATE " . self::TABLE . " SET password=:password WHERE id=:id";

        try {
            $this->executeStatement($sql, array('password' => md5($newPass), 'id' => $userid));
        } catch (\PDOException $e) {
            throw new DBException("", 4);
        }
    }

    public function delete($userid) {
        $sql = "DELETE FROM " . self::TABLE . " WHERE id=:id";
        try {
            $this->executeStatement($sql, array('id' => $userid));
        } catch (\PDOException $e) {
            throw new DBException("", 3);
        }
    }

    public function getUsuarioHabilidades($userid) {
        $sql = "SELECT habilidad.id, habilidad.habilidad " .
                "FROM habilidad " .
                "INNER JOIN has_habilidad " .
                "ON has_habilidad.habilidad_id = habilidad.id " .
                "AND has_habilidad.usuario_id = :usuario_id";

        try {
            $hasHabilidadesStatement = $this->executeStatement($sql, array('usuario_id' => $userid));
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        return $habilidades = $hasHabilidadesStatement->fetchAll();
    }

    public function getHabilidades() {
        $sql = "SELECT * FROM habilidad";
        try {
            $habilidades = $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 1);
        }

        return $habilidades->fetchAll();
    }

    public function insertHabilidades($userid, $habilidades) {
        if ($habilidades != "") {
            foreach ($habilidades as $habilidad) {
                $this->insertHabilidad($userid, $habilidad);
            }
        }
    }

    public function insertHabilidad($userid, $habilidad) {
        
        $sql = "INSERT INTO has_habilidad (usuario_id, habilidad_id) ";
        $sql .= "VALUES (" . $userid . ", " . $habilidad . "); ";

        try {
            $habilidadesStatement = $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 2);
        }
    }

    public function updateHabilidades($userid, $habilidades) {
        try {
            $this->deleteHabilidades($userid);
            $this->insertHabilidades($userid, $habilidades);
        } catch (\PDOException $e) {
            throw new DBException("", 4);
        }
    }

    public function deleteHabilidades($userid) {
        $sql = "DELETE FROM has_habilidad WHERE usuario_id = " . $userid;
        try {
            $this->executeStatement($sql);
        } catch (\PDOException $e) {
            throw new DBException("", 3);
        }
    }
}
