<?php

use Controllers\AuthorizedController;
use Model\EventoModel;
use Model\ServicioModel;
use Model\UserModel;

/**
 * Description of EventosController
 *
 * @author Lenovo
 */
class EventosController extends AuthorizedController {

    public function index($params = null) {
        $eventoModel = new EventoModel();
        $eventos = $eventoModel->getAll();
        $this->view->show('eventos/index.php', array('eventos' => $eventos));
    }

    public function add($params = null) {
        if ($params[0] == "servicio") {
            $message = $this->addServicio2Evento($params);
        } else if ($params[0] == "asignacion") {
            $message = $this->asignarTarea2Usuario($params);
        } else {
            $message = $this->addEvento($params);
        }
    }

    public function show($params = null) {
        $eventoModel = new EventoModel();
        $servicioModel = new ServicioModel();

        $eventoid = $params[0];
        $evento = $eventoModel->get($eventoid);
        $servicios = $servicioModel->getFromEvento($eventoid);

        $this->view->show('eventos/show.php', array('evento' => $evento, 'servicios' => $servicios));
    }

    public function create($params = null) {
        $eventoModel = new EventoModel();
        $servicioModel = new ServicioModel();
        $servicios = array_pop($_POST);
        $evento = $_POST;

        try {
            $eventoModel->add($evento, $servicios);
        } catch (\PDOException $e) {
            throw new \PDOException("No se pudo insertar el nuevo evento");
        }

        header('Location: ?eventos&index&success=1');
    }

    public function edit($params = null) {
        $eventoid = $params[0];
        $eventoModel = new EventoModel();
        $servicioModel = new ServicioModel();

        $evento = $eventoModel->get($eventoid);
        if (!$evento) {
            header("Location: ?eventos");
        }

        $evento->servicios = $servicioModel->getFromEvento($eventoid);
        $servicios = $servicioModel->getAll();

        $this->view->show('eventos/edit.php', array('evento' => $evento, 'servicios' => $servicios));
    }

    public function update($params = null) {
        if (isset($_POST['lugar']) && isset($_POST['fecha']) && isset($_POST['cantidad_invitados']) && isset($_POST['costo'])) {
            $evento['lugar'] = $_POST['lugar'];
            $evento['fecha'] = $_POST['fecha'];
            $evento['cantidad_invitados'] = $_POST['cantidad_invitados'];
            $evento['costo'] = $_POST['costo'];
            $evento['descripcion'] = $_POST['descripcion'];
            $evento['id'] = $_POST['id'];
            $servicios = "null";

            if (isset($_POST['servicios'])) {
                $servicios = $_POST['servicios'];
            }

            //modelo de los eventos
            $eventoModel = new EventoModel();

            try {
                $eventoModel->update($evento, $servicios);
                header("Location: ?eventos=ok");
            } catch (\PDOException $e) {
                header("Location: ?eventos&index&error=" . $e->getMessage());
            }
        }
    }

    public function delete($params = null) {
        $eventosModel = new EventoModel();
        $id = $params[0];
        try {
            $eventosModel->delete($id);
            header("Location: ?eventos");
        } catch (PDOException $e) {
            header("?eventos&index&error=" . $e->getMessage());
        }
        header("Location: ?eventos");
    }

    private function addEvento($params = null) {
        $servicioModel = new ServicioModel();
        $servicios = $servicioModel->getAll();
        $this->view->show('eventos/new.php', array('servicios' => $servicios));
    }

    public function asignar($params = null) {
        if (!isset($params)) {
            $eventoModel = new EventoModel();

            $eventos = $eventoModel->getAll();

            $this->view->show('eventos/asignacion/index.php', array("eventos" => $eventos));
        } else if ($params[0] == "add") {
            array_shift($params);
            $this->addAsignacion($params);
        } else if ($params[0] == "update") {
            $this->updateAsignacion($params);
        } else
            $this->asignar();
    }

    private function addAsignacion($params) {
        $eventoid = $params[0];
        $eventoModel = new EventoModel();
        $userModel = new UserModel();
        $evento = $eventoModel->get($eventoid);
        $tareas = $eventoModel->getTareasByEvento($eventoid);
        $empleados = $userModel->getEmpleados();
        $asignaciones = $eventoModel->getAsignacion($eventoid);
        $this->view->show("eventos/asignacion/new.php", array("evento" => $evento, "tareas" => $tareas, "empleados" => $empleados, "asignaciones" => $asignaciones));
    }

    private function updateAsignacion() {
        $eventoModel = new EventoModel();
        $asignaciones = null;
        foreach ($_POST['asignaciones'] as $asignacion) {
            if ($asignacion!="" || preg_match("[0-9][0-9]:[0-9][0-9]", $asignacion)>0) {
                $explode = explode(':', $asignacion);
                $a['evento_id ']= $_POST['evento_id'];
                $a['tarea_id'] = $explode[0];
                $a['usuario_id'] = $explode[1];
                $asignaciones[] = $a;
            }
            
        }
        if ($asignaciones != null) {
            try {
                $eventoModel->updateAsignacionTarea($asignaciones, $_POST['evento_id']);
                header("Location:?eventos=ok!");
            } catch (\PDOException $e) {
                header("Location: ?eventos&asignar=error: ".$e->getMessage());
            }            
        } else {
            header("Location: ?eventos");
        }
        
    }
    
    public function asignacion(){
        $user = $_SESSION['user'];
        
        $eventoModel = new EventoModel();
        
        $asignaciones = $eventoModel->getFullAsignacion($user->id);
        
        $this->view->show('eventos/asignacion/showempleado.php', array('asignaciones'=>$asignaciones));        
    }

    public function getAdminRules() {
        return array(
            "add",
            "create", 
            "edit", 
            "update", 
            "delete", 
            "addEvento", 
            "asignar", 
            "addAsignacion", 
            "updateAsignacion");
    }

}

?>
