<?php

//namespace Controllers;

use Libs\AbstractController;
use Model\UserModel;
use Libs\SessionInstance;

class LoginController extends AbstractController {

    //Accion index
    public function index($param = null) {
        // error de autentificación
        $error = null;
        if (!is_null($param)) {
            if (isset($_GET['error']) && $_GET['error'] == 'noauth') {
                $error = 'noauth';
            }
        }

        //si ya existe una sesion abierta, redirecciona al dashboard
        $this->is_auth();
        $this->view->show("login.php", array('no_head' => true, 'no_foot' => true, 'error' => $error));
    }

    public function auth() {
        $keys = array_keys($_POST);
        $values = array_values($_POST);

        $users = new UserModel();
        $users = $users->getAll();

        foreach ($users as $user) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password = md5($password);
            if (($user->username === $username ) && ($user->password === $password )) {
                session_start();
//                session_id(md5($username));
//                session_name($username);

                var_dump($_SESSION);
                $_SESSION["user"] = $user;
                $_SESSION["key"] = md5($user->username . $user->nombre);
                session_name($_SESSION['key']);
                header("Location:?dashboard");
            }            
        }
        
        header("Location:?login&index&error=noauth");
    }

    private function is_auth() {
        session_start();
        if (isset($_SESSION['key'])) {
            header("Location:?dashboard");
        }
    }

}

?>