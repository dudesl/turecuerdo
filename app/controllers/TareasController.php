<?php
use Controllers\FullAdminAuthController;
use Model\TareaModel;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TareasController
 *
 * @author Lenovo
 */
class TareasController extends FullAdminAuthController{
    
    public function index($params = null) {
        $tareas = new TareaModel();
        $tareas = $tareas->getAll();
        array_multisort($tareas, $tareas);
        $this->view->show('tareas/index.php', array('tareas'=>$tareas));
    }
    
    public function add(){
        
        
        if (isset($_POST['tareaname'])) {
            
            $tareaname = $_POST['tareaname'];            
            $tareadesc = $_POST['tareadesc'];            
            
            if (count_chars($tareaname)>50) {
                header("?tareas&index&error&tareaname-max");
            }
            if (count_chars($tareadesc)>200) {
                header("?tareas&index&error&tareadesc-max");
            }
            $tareas = new TareaModel();                        
            try{
                $tareas->add($tareaname, $tareadesc);
                header("Location: ?tareas=ok");
            } catch(PDOException $e){
                header("?tareas&index&error=".$e->getMessage());
            }
            
        } else{
            header("?tareas&index&error");
        }
        header("Location: ?tareas");
    }
    
    public function delete($params = null){
        if (isset($params)) {
            $id = $params[0];
            $tareas = new TareaModel();
            
            try{
                $tareas->delete($id);                
                header("Location: ?tareas=ok");
            } catch(\PDOException $e){
                header("?tareas&index&error=".$e->getMessage());
            }
        } else{
            header("?tareas");
        }
        header("Location: ?tareas");
    }
    
    public function edit($params){
        $tareaid = $params[0];        
        $tareas = new TareaModel();
        $tarea = $tareas->get($tareaid);
        
        $this->view->show('tareas/edit.php', array('tarea'=>$tarea));
    }
    
    public function update(){
        if (isset($_POST['tareaname'])) {
            
            $tareaid = $_POST['tareaid'];
            $tareaname = $_POST['tareaname'];            
            $tareadesc = $_POST['tareadesc'];            
            
            //validacion del lado del servidor del nombre de la tarea
            if (count_chars($tareaname)>50) {
                header("?tareas&index&error&tareaname-max");
            }
            
            //validacion del lado del servidor de la descripción de la tarea
            if (count_chars($tareadesc)>200) {
                header("?tareas&index&error&tareadesc-max");
            }
            
            $tareas = new TareaModel();                        
            try{
                $tareas->update($tareaid,$tareaname, $tareadesc);
                header("Location: ?tareas=ok");
            } catch(PDOException $e){
                header("?tareas&index&error=".$e->getMessage());
            }
            
        } else{
            header("?tareas&index&error");
        }
        header("Location: ?tareas");
    }
}

?>
