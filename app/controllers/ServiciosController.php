<?php

use Controllers\FullAdminAuthController;
use Model\ServicioModel;
use Model\TareaModel;

/**
 * Description of ServiciosController
 *
 * @author Lenovo
 */
class ServiciosController extends FullAdminAuthController {

    //put your code here
    public function index($params = null) {
        $servicioModel = new ServicioModel();
        $servicios = $servicioModel->getAll();

        $this->view->show('servicios/index.php', array("servicios" => $servicios));
    }

    public function add($params = null) {
        $tareaModel = new TareaModel();
        $tareas = $tareaModel->getAll();

        $this->view->show('servicios/new.php', array("tareas" => $tareas));
    }

    public function create($params = null) {
        $servicioModel = new ServicioModel();
        $tareas = array_pop($_POST);
        $servicio = $_POST;
        try {
            $servicioModel->add($servicio, $tareas);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage());
        }
        
        header('Location: ?servicios&index&success=1');
    }

    public function edit($params = null) {
        $servicioid = $params[0];
        $servicioModel = new ServicioModel();
        $tareaModel = new TareaModel();

        $servicio = $servicioModel->get($servicioid);
        if (!$servicio) {
            header("Location: ?servicios");
        }

        $servicio->tareas = $servicioModel->getTareasByServicio($servicioid);
        $tareas = $tareaModel->getAll();

        $this->view->show('servicios/edit.php', array('servicio' => $servicio, 'tareas' => $tareas));
    }
    
    public function update($params = null){
        if (isset($_POST['nombre']) ) {
            $servicio['nombre'] = $_POST['nombre'];
            $servicio['descripcion'] = $_POST['descripcion'];
            $servicio['id'] = $_POST['id'];
            $tareas = "null";
            
            if (isset($_POST['tareas'])) {
                $tareas = $_POST['tareas'];
            }

            //modelo de los eventos
            $servicioModel = new ServicioModel();

            try {
                $servicioModel->update($servicio, $tareas);
                header("Location: ?servicios=ok");
            } catch (\PDOException $e) {
                header("Location: ?servicios&index&error=" . $e->getMessage());
            }
        }
    }
    
    public function delete($params = null){
        $servicioModel = new ServicioModel();
        $id = $params[0];
        try {
            $servicioModel->delete($id);
            header("Location: ?servicios=ok");
        } catch (PDOException $e) {
            header("?servicios&index&error=" . $e->getMessage());
        }
        header("Location: ?servicios");
    }
}

?>
