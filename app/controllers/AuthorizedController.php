<?php

namespace Controllers;

use Libs\AbstractController;
use Libs\View;
use Libs\IAuthorizedController;

/**
 * Clase padre de todos los controladores que necesitan autorización para ser accedidos 
 */
abstract class AuthorizedController extends AbstractController implements IAuthorizedController {

    protected $view;

    const ROLADMIN = "Administrador";
    const ROL2 = "Empleado";

    public function __construct() {
        session_start();
        
        //si no existe session, destruyo las sesiones actuales y redirecciono al login
        if ($_SESSION['key'] == null) {
            $this->close();
        }

        $this->view = new View();
    }

    /**
     * Logout del sistema 
     */
    public function close() {
        //destruyo variables de sesion
        $_SESSION = null;
        
        //destruyo sesion
        session_destroy();
        
        //redirecciono al comienzo de la aplicacion
        header("Location:?");
    }
    
    public function beforeAction($action, $actionParams = null) {}

    public function afterAction($action, $actionParams = null) {}

    public function _call($action, $actionParams = null) {
        if ($this->isAuth($action)) {
            $this->beforeAction($action, $actionParams);
            $this->$action($actionParams);
            $this->afterAction($action, $actionParams);
        } else $this->noAuth();
    }

    public function isAuth($action) {
        $rules = $this->getAdminRules();
        $hasRule = array_search($action, $rules)? true : false;        
        if ($hasRule) return $_SESSION['user']->rol === self::ROLADMIN ? true : false;
        return true;
    }
    
    public function noAuth(){
        header("Location: ?error&noauth");
    }
    
    public function getAdminRules(){ 
        return array();        
    }

}

?>
