<?php

use Controllers\AuthorizedController;

/**
 * Description of Error
 *
 * @author dudesl
 */
class ErrorController extends AuthorizedController{    
    
    public function e404(){
        $this->view->showWhitoutHeadFoot('error/404.php');        
    }

    public function index($params = null) {        
    }
    
    public function noauth(){
        $this->view->show('error/noauth.php');
    }
}

?>
