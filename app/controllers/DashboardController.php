<?php
use Controllers\AuthorizedController;
use Model\UserModel;

class DashboardController extends AuthorizedController{
    
    public function index($params = null){
        $this->view->show("dashboard.php");
    }
    
}

?>
