<?php

use Controllers\AuthorizedController;
use Model\UserModel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TareasController
 *
 * @author Lenovo
 */
class RrhhController extends AuthorizedController {

    public function index($params = null) {
        $userModel = new UserModel();
        $users = $userModel->getAll();
        $this->view->show('rrhh/index.php', array('users' => $users));
    }

    public function show($params = null) {
        $userModel = new UserModel();


        $userid = $params[0];

        $user = $userModel->get($userid);

        if (!$user) {
            header("Location: ?rrhh");
        }

        $habilidades = $userModel->getUsuarioHabilidades($userid);

        $this->view->show('rrhh/show.php', array('user' => $user, 'habilidades' => $habilidades));


        /** Obtengo el usuario y sus habilidades * */
    }

    public function add($params = null) {
        $userModel = new UserModel();
        $users = $userModel->getAll();

        $habilidades = $userModel->getHabilidades();
        $this->view->show('rrhh/new.php', array("habilidades" => $habilidades));
    }

    public function create($params = null) {
        if (isset($_POST['username']) && isset($_POST['password']) &&
                isset($_POST['mail']) && isset($_POST['rol'])) {
            $user['username'] = $_POST['username'];
            $user['password'] = $_POST['password'];
            $user['nombre'] = $_POST['nombre'];
            $user['direccion'] = $_POST['direccion'];
            $user['telefono'] = $_POST['telefono'];
            $user['mail'] = $_POST['mail'];
            $user['rol'] = $_POST['rol'];
            $user['habilidades'] = "";

            if (isset($_POST['habilidades'])) {
                $user['habilidades'] = $_POST['habilidades'];
            }

            //modelo de los usuarios
            $userModel = new UserModel();

            try {
                $userModel->add($user);
            } catch (\PDOException $e) {
                header("Location: ?rrhh&index&error=" . $e->getMessage());
            }
            header("Location: ?rrhh=ok");
        }
    }

    public function delete($params = null) {
        if (isset($params)) {
            $id = $params[0];
            //El usuario no se puede eliminar a si mismo
            if ($_SESSION['user']->id != $id) {
                $usersModel = new UserModel();

                try {
                    $usersModel->delete($id);
                    header("Location: ?rrhh=ok");
                } catch (PDOException $e) {
                    header("?tareas&index&error=" . $e->getMessage());
                }
            }
        }
        header("Location: ?rrhh");
    }

    public function edit($params = null) {
        //Obtengo el usuario que quiero editar
        $userid = $params[0];
        $userModel = new UserModel();
        $user = $userModel->get($userid);

        //si no existe el usuario, redirecciono al inicio de la seccion
        if (!$user) {
            header("Location: ?rrhh");
        }

        //Obtengo todas las habilidades
        $habilidades = $userModel->getHabilidades();

        //Obtengo las habilidades del usuario
        $habilidades_usuario = $userModel->getUsuarioHabilidades($userid);


        $user->habilidades = $habilidades_usuario;

        //llamo a la vista
        $this->view->show('rrhh/edit.php', array('edit' => true, 'user' => $user, 'habilidades' => $habilidades));
    }

    public function update($params = null) {
        if ($params && $params[0] == 'password') {
            $this->updatePassword();
        } else if (isset($_POST['username']) && isset($_POST['mail']) && isset($_POST['nombre']) && isset($_POST['rol'])) {
            $user['username'] = $_POST['username'];
            $user['nombre'] = $_POST['nombre'];
            $user['direccion'] = $_POST['direccion'];
            $user['telefono'] = $_POST['telefono'];
            $user['mail'] = $_POST['mail'];
            $user['rol'] = $_POST['rol'];
            $user['id'] = $_POST['id'];

            $habilidades = "null";

            if (isset($_POST['habilidades'])) {
                $habilidades = $_POST['habilidades'];
            }

            //modelo de los usuarios
            $userModel = new UserModel();

            try {
                $userModel->update($user, $habilidades);
                header("Location: ?rrhh=ok");
            } catch (\PDOException $e) {
                header("Location: ?rrhh&index&error=" . $e->getMessage());
            }
        }
        else
            header("Location:?rrhh");
    }

    public function updatePassword() {
        $userModel = new UserModel();

        try {
            $userModel->updatePass($_POST['password'], $_POST['id']);
        } catch (\PDOException $e) {
            echo $e->getTraceAsString();
        }

        header("Location: ?rrhh");
    }

}

?>
