<?php

use Controllers\AuthorizedController;
use Model\UserModel;

class PerfilController extends AuthorizedController {

    public function index($params = null) {
        $userModel = new UserModel();

        $userid = $_SESSION['user']->id;
        $user = $userModel->get($userid);

        $this->view->show('perfil/index.php', array('user' => $user));
    }

    public function edit($params = null) {
        if ($params[0] == "password") {
            array_shift($params);
            $this->editPassword($params);
        } else {
            $userid = $params[0];
            $userModel = new UserModel();
            $user = $userModel->get($userid);
            $this->view->show("perfil/edit.php", array('user' => $user));
        };
    }

    public function update($params = null) {
        if ($params[0] == "password") {
            array_shift($params);
            $this->updatePassword($params);
        } else {
            if ($params && $params[0] == 'password') {
                $this->updatePassword();
            } else if (isset($_POST['username']) && isset($_POST['mail']) && isset($_POST['nombre']) && isset($_POST['rol'])) {
                $user['username'] = $_POST['username'];
                $user['nombre'] = $_POST['nombre'];
                $user['direccion'] = $_POST['direccion'];
                $user['telefono'] = $_POST['telefono'];
                $user['mail'] = $_POST['mail'];
                $user['rol'] = $_POST['rol'];
                $user['id'] = $_POST['id'];

//modelo de los usuarios
                $userModel = new UserModel();

                try {
                    $userModel->update($user);
                    $_SESSION['user'] = $userModel->get($user['id']);
                } catch (\PDOException $e) {
                    header("Location: ?perfil=error: " . $e->getMessage());
                }

                header("Location: ?perfil=ok");
            }
            else
                header("Location:?perfil");
        };
    }

    public function updatePassword($params) {

        $userid = $_SESSION['user']->id;
        $password = $_POST['password'];

        $userModel = new UserModel();

        try {
            $userModel->updatePass($password, $userid);
        } catch (\PDOException $e) {
            header("Location: ?perfil=error: " . $e->getMessage());
        }

        header("Location: ?perfil=ok");
    }

}

?>
