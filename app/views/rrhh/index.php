<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li class="active"><span>Empleados</span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('rrhh') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">

                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/list.png" alt="" />
                                Listado de usuarios
                            </span>
                        </div>
                        <div class="da-panel-toolbar top">
                                    <ul>
                                        <li><a href="?rrhh&add"><img src="images/icons/color/pencil.png" alt="">Crear Usuario</a></li>
                                    </ul>
                                </div>
                        <div class="da-panel-content">
                            <table class="da-table datatable" id="rrhh-datatable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nick</th>
                                        <th>Tel&eacute;fono</th>
                                        <th>Mail</th>                                        
                                        <th>Rol</th>                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $u): ?>
                                        <tr>
                                            <td><?php echo $u->id ?></td>
                                            <td><?php echo $u->nombre ?> (<?php echo $u->username ?>)</td>
                                            
                                            <td><?php echo $u->telefono ?></td>
                                            <td><?php echo $u->mail ?></td>
                                            <td><?php echo $u->rol ?></td>
                                            <td class="da-icon-column">
                                                <a href="?rrhh&show&<?php echo $u->id ?>"><img src="images/icons/color/magnifier.png" /></a>
                                                <a href="?rrhh&edit&<?php echo $u->id ?>"><img src="images/icons/color/pencil.png" /></a>
                                                <a href="?rrhh&delete&<?php echo $u->id ?>" class="delete"><img src="images/icons/color/cross.png" /></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>                              
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
