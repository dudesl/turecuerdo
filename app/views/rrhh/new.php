<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li><a href="?rrhh">Usuarios</a></li>
                <li class="active"><span>Agregar Usuario</span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('rrhh') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">
                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/pencil.png" alt="">
                                Agregar nuevo usuario
                            </span>
                        </div>
                        <div class="da-panel-content">
                            <form class="da-form" action="?rrhh&create" method="POST" id="rrhh-add-form">
                                <div id="rrhh-add-error" class="da-message error" style="display:none"></div>
                                <fieldset>
                                    <legend>Datos de Autentificaci&oacute;n</legend>
                                    <div class="da-form-row">
                                        <label>Usuario (nick)<span class="required">*</span></label>                                                    
                                        <div class="da-form-item large">
                                            <span class="formNote">Ingrese un nombre de usuario</span>
                                            <input type="text" name="username">
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Rol<span class="required">*</span></label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Seleccione el rol del usuario en el sistema</span>
                                            <select name="rol">
                                                <option value="Empleado" selected="selected">Empleado</option>
                                                <option value="Administrador">Administrador</option>
                                            </select>                                            
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Contrase&ntilde;a<span class="required">*</span></label>                                                    
                                        <div class="da-form-item large">
                                            <span class="formNote">Ingrese una contrase&ntilde;a</span>
                                            <input type="password" name="password" id="password">
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Repita contrase&ntilde;a<span class="required">*</span></label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Repita la contrase&ntilde;a</span>
                                            <input type="password" name="password2">
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Datos del usuario</legend>

                                    <div class="da-form-row">
                                        <label>Nombre del usuario<span class="required">*</span></label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Ingrese un nombre de usuario</span>
                                            <input type="text" name="nombre">
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Direcci&oacute;n</label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Ingrese una direcci&oacute;n del usuario</span>
                                            <input type="text" name="direccion">
                                        </div>
                                    </div>                                    
                                    <div class="da-form-row">
                                        <label>Tel&eacute;fono</label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Ingrese un tel&eacute;fono</span>
                                            <input type="text" name="telefono">
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Correo Electr&oacute;nico<span class="required">*</span></label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Ingrese un correo electr&oacute;nico</span>
                                            <input type="text" name="mail">
                                        </div>
                                    </div>                                                                                                                    
                                </fieldset> 
                                <fieldset>
                                    <legend>Habilidades</legend>
                                    <div class="da-form-row da-form-block">
                                        <div class="da-form-item large">
                                            <span class="formNote">Escoja de la lista por lo menos una habilidad</span>
                                            <select id="habilidades-chozen" name ="habilidades[]" multiple="multiple" id="habilidades">
                                                <?php foreach ($habilidades as $habilidad): ?>
                                                    <option value="<?php echo $habilidad['id'] ?>">
                                                        <?php echo $habilidad['habilidad'] ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="da-button-row">
                                    <input type="reset" value="Resetear" class="da-button gray left">
                                    <input type="submit" value="Guardar" class="da-button green">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
