<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li><a href="?rrhh">Usuarios</a></li><li class="active"><span><?php echo $user->nombre ?></span></li>

            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('rrhh') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">

                <div class="grid_4">
                    <div class="da-panel collapsible">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/color/blog.png" alt="" />
                                Vista detallada de <?php echo $user->nombre ?>
                            </span>

                        </div>
                        <div class="da-panel-toolbar top">
                            <ul>
                                <li><a href="?rrhh&edit&<?php echo $user->id ?>"><img src="images/icons/color/pencil.png" alt="" />Editar</a></li>
                                <li><a href="?rrhh&delete&<?php echo $user->id ?>" class="delete"><img src="images/icons/color/cross.png" alt="" />Borrar</a></li>                                
                            </ul>
                        </div>      
                        <div class="da-panel-content">
                            <table class="da-table da-detail-view">
                                <tbody>
                                    <tr>
                                        <th>Usuario</th>
                                        <td><?php echo $user->nombre ?> (<?php echo $user->username ?>)</td>
                                    </tr>
                                    <tr>
                                        <th>Rol</th>
                                        <td><?php echo $user->rol ?></td>
                                    </tr>
                                    <tr>
                                        <th>Direccion</th>
                                        <td><?php echo $user->direccion ?></td>
                                    </tr>
                                    <tr>
                                        <th>Tel&eacute;fono</th>
                                        <td><?php echo $user->telefono ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?php echo $user->mail ?></td>
                                    </tr>
                                    <tr>
                                        <th>Habilidades</th>
                                        <td><?php foreach ($habilidades as $habilidad): ?>
                                                <?php echo $habilidad['habilidad'] ?>.  
                                            <?php endforeach ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>
