             <div id="da-header-bottom">
                <!-- Container -->
                <div class="da-container clearfix">
                	
                    <!-- Breadcrumbs -->
                    <div id="da-breadcrumb">
                        <ul>
                            <li class="active"><span><img src="images/icons/black/16/home.png" alt="Home" />Tablero</span></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    
        <!-- Content -->
        <div id="da-content">
            
            <!-- Container -->
            <div class="da-container clearfix">
                
                <!-- Sidebar Separator do not remove -->
                <div id="da-sidebar-separator"></div>
                
                <?php sidebar('')?>
                <!-- Main Content Wrapper -->
                <div id="da-content-wrap" class="clearfix">
                
                	<!-- Content Area -->
                	<div id="da-content-area">
                            <div class="grid_4">
                                <div class="da-panel">
                                    <div class="da-panel-header">
                                            <span class="da-panel-title">
                                            <img src="images/icons/color/key.png" alt="" />
                                            Hey! ¿ Donde vas ?
                                        </span>

                                    </div>
                                    <div class="da-panel-content">
                                        <div class="da-message warning">
                                            No est&aacute; autorizado para realizar esta acci&oacute;n
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
        
