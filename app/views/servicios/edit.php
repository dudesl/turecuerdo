<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li><a href="?servicios">Servicios</a></li>
                <li class="active"><span>Servicios N&ordm; <?php echo $servicio->id ?></span></li>
            </ul>
        </div>
    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('servicio') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">
                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/list.png" alt="" />
                                Editar Servicios N&ordm; <?php echo $servicio->id ?>
                            </span>
                        </div>
                        <div class="da-panel-content">
                            <form class="da-form" action="index.php?servicios&update" id="tarea-add-form" method="POST" name="servicio">
                                <input type="hidden" value="<?php echo $servicio->id ?>" name="id" />
                                <div id="servicio-add-error" class="da-message error" style="display:none"></div>
                                <div class="da-form-inline">
                                    <div class="da-form-row">
                                        <label for="tarea-nombre">Nombre del servicio<span class="required">*</span></label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Escriba un nombre descriptivo del servicio</span>
                                            <input type="text" name="nombre" value="<?php echo $servicio->nombre ?>"/>
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Descripci&oacute;n</label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Escriba una descripci&oacute;n del servicio</span>
                                            <textarea rows="auto" cols="auto" name="descripcion"><?php echo $servicio->descripcion ?></textarea>
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Tareas</label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Escoja de la lista por lo menos una tarea</span>
                                            <select class="chozen" id="tareas-chozen" name ="tareas[]" multiple="multiple" id="tareas">
                                                <?php foreach ($tareas as $tarea): ?>

                                                <option 
                                                <?php echo "value=\"" . $tarea->id . "\"" ?> 
                                                <?php
                                                if (isset($servicio->tareas) && (count($servicio->tareas) > 0)) {
                                                    foreach ($servicio->tareas as $tareaServicio) {
                                                        if ($tareaServicio->id == $tarea->id)
                                                            echo "selected=\"selected\"";
                                                    }
                                                }
                                                ?>                                                   
                                                    >
                                                        <?php echo $tarea->nombre ?>                                                        
                                                </option>
                                            <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="da-button-row">
                                        <input type="submit" value="Guardar" class="da-button green">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
