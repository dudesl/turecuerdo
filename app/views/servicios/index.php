<?php
$no_servicios = true;
if (isset($servicios) && count($servicios> 0)) {
    $no_servicios = false;
}
?>
<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li class="active"><span>Servicios</span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('servicio') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">

                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/list.png" alt="" />
                                Servicios
                            </span>
                        </div>
                        <div class="da-panel-toolbar top">
                            <ul>
                                <li><a href="?servicios&add"><img src="images/icons/color/pencil.png" alt="" />Agregar Servicio</a></li>                                
                            </ul>
                        </div> 
                        <?php if (!$no_servicios): ?>       
                        <div class="da-panel-content">
                            <table class="da-table datatable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Servicio</th>
                                        <th>Descripci&oacute;n</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($servicios as $servicio): ?>
                                        <tr>
                                            <td><?php echo $servicio->id ?></td>
                                            <td><?php echo $servicio->nombre ?></td>
                                            <td><?php echo $servicio->descripcion ?></td>
                                            <td class="da-icon-column">
                                                <a href="?servicios&edit&<?php echo $servicio->id ?>"><img src="images/icons/color/pencil.png" /></a>
                                                <a href="?servicios&delete&<?php echo $servicio->id ?>" class="delete"><img src="images/icons/color/cross.png" /></a>
                                            </td>
                                        </tr>    
                                    <?php endforeach; ?>                                    
                                </tbody>
                            </table>                            
                        </div>
                        <?php else : ?>
                        <div class="da-panel-content">
                            <div class="da-message warning">
                                No hay ningun servicio en la base de datos. Por favor, cargue uno.
                            </div>    
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
