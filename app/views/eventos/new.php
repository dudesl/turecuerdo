<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li><a href="?eventos">Eventos</a></li>
                <li class="active"><span>Agregar Evento</span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('evento') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">
                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/pencil.png" alt="">
                                Agregar nuevo evento
                            </span>
                        </div>
                        <div class="da-panel-content">
                            <form class="da-form" action="?eventos&create" method="POST" id="evento-add-form">
                                <div id="evento-add-error" class="da-message error" style="display:none"></div>
                                <div class="da-form-row">
                                    <label>Lugar<span class="required">*</span></label>                                                    
                                    <div class="da-form-item large">
                                        <span class="formNote">Ingrese lugar donde se realizar&aacute; el evento</span>
                                        <input type="text" name="lugar">
                                    </div>
                                </div>
                                <div class="da-form-row">
                                    <label>Fecha y Hora<span class="required">*</span></label>
                                    <div class="da-form-item large">
                                        <span class="formNote">Seleccione la fecha y el horario del evento</span>
                                        <input type="text" name="fecha" id="datetimepicker" />
                                    </div>
                                </div>
                                <div class="da-form-row">
                                    <label>Cantidad de Invitados<span class="required">*</span></label>
                                    <div class="da-form-item">
                                        <input type="text" id="evento-invitados" name="cantidad_invitados" class="spinner" value="0"/>
                                    </div>
                                </div>
                                <div class="da-form-row">
                                    <label>Costo ($)<span class="required">*</span></label>
                                    <div class="da-form-item">
                                        <input type="text" id="evento-costo" name="costo" class="spinner" value="0"/>
                                    </div>
                                </div> 
                                <div class="da-form-row">
                                    <label>Descripci&oacute;n</label>
                                    <div class="da-form-item">
                                        <textarea name="descripcion"></textarea>
                                    </div>
                                </div> 
                                    
                                    <div class="da-form-row da-form-block">
                                        <label>Servicios</label>
                                        <div class="da-form-item large">
                                            <span class="formNote">Escoja de la lista por lo menos un servicio</span>
                                            <select id="servicios-chozen" name ="servicios[]" multiple="multiple" id="servicios">
                                                <?php foreach ($servicios as $servicio): ?>
                                                    <option value="<?php echo $servicio->id ?>">
                                                        <?php echo $servicio->nombre ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="da-button-row">
                                    <input type="reset" value="Resetear" class="da-button gray left">
                                    <input type="submit" value="Guardar" class="da-button green">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
