

<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li><a href="?eventos">Eventos</a></li>
                <li class="active"><span>Asignaci&oacute;n de tareas</span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('evento') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">


                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/list.png" alt="" />
                                Asignaci&oacute;n de tareas. Evento N&ordm; <?php echo $evento->id ?>
                            </span>

                        </div>

                        <div class="da-panel-content">
                            <table id="da-ex-datatable-default" class="da-table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Lugar</th>
                                        <th>Fecha</th>
                                        <th>Invitados</th>
                                        <th>Costo</th>                                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $evento->id ?></td>
                                        <td><?php echo $evento->lugar ?></td>
                                        <td><?php echo $evento->fecha ?></td>
                                        <td><?php echo $evento->cantidad_invitados ?></td>                                            
                                        <td>$ <?php echo $evento->costo ?></td>                                                
                                    </tr>                                        
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/pencil.png" alt="">
                                Tareas del evento
                            </span>
                        </div>
                        <div class="da-panel-content">
                            <form class="da-form" action="?eventos&asignar&update" method="POST" id="evento-add-form">
                                <input type="hidden" value="<?php echo $evento->id ?>" name="evento_id" />
                                <div id="evento-add-error" class="da-message error" style="display:none"></div>
                                <?php foreach ($tareas as $tarea): ?>
                                    <div class="da-form-row">
                                        <label><?php echo $tarea->nombre ?></label>                                                    
                                        <div class="da-form-item">
                                            <span class="formNote">Seleccione un empleado para realizar la tarea en este evento</span>
                                            <select name="asignaciones[]" class="chozen" multiple="multiple">
                                                <option></option>
                                                <?php foreach ($empleados as $empleado): ?>
                                                    <option 
                                                        value="<?php echo $tarea->id ?>:<?php echo $empleado->id ?>"
                                                        <?php if (count($asignaciones) > 0): ?>
                                                            <?php
                                                            foreach ($asignaciones as $asignacion):
                                                                if ($asignacion->usuario_id == $empleado->id &&
                                                                        $asignacion->tarea_id == $tarea->id):
                                                                    ?>
                                                                    selected="selected"
                                                                <?php endif ?>
                                                            <?php endforeach; ?>
                                                    <?php endif ?>>
                                                            <?php echo $empleado->nombre ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endforeach; ?>  
                                <div class="da-button-row">
                                    <input type="reset" value="Resetear" class="da-button gray left">
                                    <input type="submit" value="Guardar" class="da-button green">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
