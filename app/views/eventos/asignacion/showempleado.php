<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li><a href="?eventos">Asignaci&oacute;n de eventos</a></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('tablero') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">
                <?php if (isset($asignaciones)): ?>
                    <?php foreach ($asignaciones as $evento): ?>
                        <?php /* TODO: mostrar solo eventos venideros, no los pasados */ ?>
                        <div class="grid_4">
                            <div class="da-panel collapsible">
                                <div class="da-panel-header">
                                    <span class="da-panel-title">
                                        <img src="images/icons/color/blog.png" alt="" />
                                        Vista detallada del evento N&ordm; <?php echo $evento->id ?>
                                    </span>

                                </div>      
                                <div class="da-panel-content">
                                    <table class="da-table da-detail-view">
                                        <tbody>
                                            <tr>
                                                <th>Lugar</th>
                                                <td><?php echo $evento->lugar ?></td>
                                            </tr>
                                            <tr>
                                                <th>Fecha</th>
                                                <td><?php echo $evento->fecha ?></td>
                                            </tr>
                                            <tr>
                                                <th>Cantidad de Invitados</th>
                                                <td><?php echo $evento->cantidad_invitados ?></td>
                                            </tr>
                                            <tr>
                                                <th>Costo</th>
                                                <td>$ <?php echo $evento->costo ?></td>
                                            </tr>
                                            <tr>
                                                <th>Descripci&oacute;n</th>
                                                <td><?php echo $evento->descripcion ?></td>
                                            </tr>
                                            <tr>
                                                <th>Tareas Asignadas</th>
                                                <td>
                                                    <ul>
                                                        <?php foreach ($evento->tareas as $tarea): ?>
                                                            <li>
                                                                <?php echo $tarea->nombre ?>
                                                                <p><?php echo $tarea->descripcion ?></p>
                                                            </li>
                                                        <?php endforeach ?>
                                                    </ul> 
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                <?php else: ?>
                    <div class="grid_4">
                        <div class="da-message success">
                            No tienes ninguna asignaci&oacute;n de tareas!
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>