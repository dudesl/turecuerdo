<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li><a href="?eventos">Eventos</a></li>
                <li class="active"><span>Editar Evento N&ordm; <?php echo $evento->id ?></span></li>
            </ul>
        </div>
    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('evento') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">
                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/pencil.png" alt="">
                                Editar Evento N&ordm; <?php echo $evento->id ?>
                            </span>
                        </div>
                        <div class="da-panel-content">
                            <form class="da-form" action="?eventos&update" method="POST" id="evento-add-form">
                                <div id="evento-add-error" class="da-message error" style="display:none"></div>
                                <input type="hidden" value="<?php echo $evento->id ?>" name="id" />
                                <div id="evento-add-error" class="da-message error" style="display:none"></div>
                                <div class="da-form-row">
                                    <label>Lugar<span class="required">*</span></label>                                                    
                                    <div class="da-form-item large">
                                        <span class="formNote">Ingrese lugar donde se realizar&aacute; el evento</span>
                                        <input type="text" name="lugar" value="<?php echo $evento->lugar ?>">
                                    </div>
                                </div>
                                <div class="da-form-row">
                                    <label>Fecha y Hora<span class="required">*</span></label>
                                    <div class="da-form-item large">
                                        <span class="formNote">Seleccione la fecha y el horario del evento</span>
                                        <input type="text" name="fecha" id="datetimepicker" value="<?php echo $evento->fecha ?>"/>
                                    </div>
                                </div>
                                <div class="da-form-row">
                                    <label>Cantidad de Invitados<span class="required">*</span></label>
                                    <div class="da-form-item">
                                        <input type="text" id="evento-invitados" name="cantidad_invitados" class="spinner" value="<?php echo $evento->cantidad_invitados ?>"/>
                                    </div>
                                </div>
                                <div class="da-form-row">
                                    <label>Costo ($)<span class="required">*</span></label>
                                    <div class="da-form-item">
                                        <input type="text" id="evento-costo" name="costo" class="spinner" value="<?php echo $evento->costo ?>"/>
                                    </div>
                                </div> 
                                <div class="da-form-row">
                                    <label>Descripci&oacute;n<span class="required"></span></label>
                                    <div class="da-form-item">
                                        <textarea name="descripcion"><?php echo $evento->descripcion ?></textarea>
                                    </div>
                                </div> 

                                <div class="da-form-row da-form-block">
                                    <label>Servicios</label>
                                    <div class="da-form-item large">
                                        <span class="formNote">Escoja de la lista por lo menos un servicio</span>
                                        <select id="servicios-chozen" name ="servicios[]" multiple="multiple">
                                            <?php foreach ($servicios as $servicio): ?>

                                                <option 
                                                <?php echo "value=\"" . $servicio->id . "\"" ?> 
                                                <?php
                                                if (isset($evento->servicios) && (count($evento->servicios) > 0)) {
                                                    foreach ($evento->servicios as $eventoServicio) {
                                                        if ($eventoServicio->id == $servicio->id)
                                                            echo "selected=\"selected\"";
                                                    }
                                                }
                                                ?>                                                   
                                                    >
                                                        <?php echo $servicio->nombre ?>                                                        
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>                                
                                <div class="da-button-row">
                                    <input type="submit" value="Guardar" class="da-button green">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
