<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li><a href="?eventos">Eventos</a></li><li class="active"><span><?php echo $evento->lugar . " " . $evento->fecha ?></span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('eventos') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">
                <div class="grid_4">
                    <div class="da-panel collapsible">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/color/blog.png" alt="" />
                                Vista detallada del evento N&ordm; <?php echo $evento->id ?>
                            </span>

                        </div>
                        <div class="da-panel-toolbar top">
                            <ul>
                                <li><a href="?eventos&edit&<?php echo $evento->id ?>"><img src="images/icons/color/pencil.png" alt="" />Editar</a></li>
                                <li><a href="?eventos&delete&<?php echo $evento->id ?>" class="delete"><img src="images/icons/color/cross.png" alt="" />Borrar</a></li>                                
                                <li><a href="?eventos&asignar&add&<?php echo $evento->id ?>"><img src="images/icons/color/user.png" alt="" />Asignar Tareas</a></li>                                
                            </ul>
                        </div>      
                        <div class="da-panel-content">
                            <table class="da-table da-detail-view">
                                <tbody>
                                    <tr>
                                        <th>Lugar</th>
                                        <td><?php echo $evento->lugar ?></td>
                                    </tr>
                                    <tr>
                                        <th>Fecha</th>
                                        <td><?php echo $evento->fecha ?></td>
                                    </tr>
                                    <tr>
                                        <th>Cantidad de Invitados</th>
                                        <td><?php echo $evento->cantidad_invitados ?></td>
                                    </tr>
                                    <tr>
                                        <th>Costo</th>
                                        <td>$ <?php echo $evento->costo ?></td>
                                    </tr>
                                    <tr>
                                        <th>Descripci&oacute;n</th>
                                        <td><?php echo $evento->descripcion ?></td>
                                    </tr>
                                    <tr>
                                        <th>Servicios</th>
                                        <td>
                                            <ul>
                                                <?php foreach ($servicios as $servicio): ?>
                                                    <li>
                                                        <?php echo $servicio->nombre ?>
                                                        <p><?php echo $servicio->descripcion ?></p>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>