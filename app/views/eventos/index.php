<?php
$no_events = true;
if (isset($eventos) && count($eventos > 0)) {
    $no_events = false;
}
?>

<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li class="active"><span>Eventos</span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('evento') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">

                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/list.png" alt="" />
                                Eventos
                            </span>

                        </div>
                        <div class="da-panel-toolbar top">
                            <ul>
                                <li><a href="?eventos&add"><img src="images/icons/color/pencil.png" alt="" />Agregar Evento</a></li>                                
                            </ul>
                        </div> 
                        <?php if (!$no_events): ?>                        
                            <div class="da-panel-content">
                                <table id="da-ex-datatable-default" class="da-table datatable">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Lugar</th>
                                            <th>Fecha</th>
                                            <th>Invitados</th>
                                            <th>Costo</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($eventos as $evento): ?>
                                            <tr>
                                                <td><?php echo $evento->id ?></td>
                                                <td><?php echo $evento->lugar ?></td>
                                                <td><?php echo $evento->fecha ?></td>
                                                <td><?php echo $evento->cantidad_invitados ?></td>                                            
                                                <td>$ <?php echo $evento->costo ?></td>
                                                <td class="da-icon-column">
                                                    <a href="?eventos&show&<?php echo $evento->id ?>"><img src="images/icons/color/magnifier.png" /></a>
                                                    <a href="?eventos&edit&<?php echo $evento->id ?>"><img src="images/icons/color/pencil.png" /></a>
                                                    <a href="?eventos&delete&<?php echo $evento->id ?>" class="delete"><img src="images/icons/color/cross.png" /></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php else : ?>
                            <div class="da-panel-content">
                                <div class="da-message warning">
                                    No hay ningun evento en la base de datos. Por favor, cargue uno, utilizando el boton de arriba                                    
                                </div>    
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>
