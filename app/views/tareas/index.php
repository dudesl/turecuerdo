<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li class="active"><span>Tareas</span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('tarea') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">

                <div class="grid_4">
                    <div class="da-panel collapsible">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/list.png" alt="" />
                                Tareas
                            </span>

                        </div>
                        <div class="da-panel-content">
                            <table class="da-table datatable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Tarea</th>
                                        <th>Descripci&oacute;n</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tareas as $tarea): ?>
                                        <tr>
                                            <td><?php echo $tarea->id ?></td>
                                            <td><?php echo $tarea->nombre ?></td>
                                            <td><?php echo $tarea->descripcion ?></td>
                                            <td class="da-icon-column">
                                                <a href="?tareas&edit&<?php echo $tarea->id ?>"><img src="images/icons/color/pencil.png" /></a>
                                                <a href="?tareas&delete&<?php echo $tarea->id ?>" class="delete"><img src="images/icons/color/cross.png" /></a>
                                            </td>
                                        </tr>    
                                    <?php endforeach; ?>                                    
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                </div>
                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/pencil.png" alt="" />
                                Agregar Tarea
                            </span>

                        </div>
                        <div class="da-panel-content">
                            <form class="da-form" action="index.php?tareas&add" id="tarea-add-form" method="POST" name="tareas">
                                <div id="tarea-add-error" class="da-message error" style="display:none"></div>
                                <div class="da-form-inline">
                                    <div class="da-form-row">
                                        <label for="tarea-nombre">Nombre de la tarea<span class="required">*</span></label>
                                        <div class="da-form-item">
                                            <span class="formNote">Escriba un nombre descriptivo de la tarea</span>
                                            <input type="text" name="tareaname"/>
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Descripci&oacute;n</label>
                                        <div class="da-form-item">
                                            <span class="formNote">Escriba una descripci&oacute;n de la tarea</span>
                                            <textarea rows="auto" cols="auto" name="tareadesc"></textarea>
                                        </div>
                                    </div>
                                    <div class="da-button-row">
                                        <input type="reset" value="Resetear" class="da-button gray left">
                                        <input type="submit" value="Guardar" class="da-button green">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
