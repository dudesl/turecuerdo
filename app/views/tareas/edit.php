<div id="da-header-bottom">
    <!-- Container -->
    <div class="da-container clearfix">
        <!-- Breadcrumbs -->
        <div id="da-breadcrumb">
            <ul>
                <li><a href="?dashboard"><img src="images/icons/black/16/home.png" alt="Inicio" />Tablero</a></li>
                <li class="active"><span>Tareas</span></li>
            </ul>
        </div>

    </div>
</div>
</div>

<!-- Content -->
<div id="da-content">

    <!-- Container -->
    <div class="da-container clearfix">

        <!-- Sidebar -->
        <div id="da-sidebar-separator"></div>
        <?php sidebar('tarea') ?>
        <!-- Main Content Wrapper -->
        <div id="da-content-wrap" class="clearfix">

            <!-- Content Area -->
            <div id="da-content-area">

                <div class="grid_4">
                    <div class="da-panel">
                        <div class="da-panel-header">
                            <span class="da-panel-title">
                                <img src="images/icons/black/16/pencil.png" alt="" />
                                Editar Tarea
                            </span>

                        </div>
                        <div class="da-panel-content">
                            <form class="da-form" action="index.php?tareas&update" id="tarea-add-form" method="POST" name="tareas">
                                <div id="tarea-add-error" class="da-message error" style="display:none"></div>
                                <div class="da-form-inline">
                                    <div class="da-form-row">
                                        <label for="tarea-nombre">Nombre de la tarea<span class="required">*</span></label>
                                        <div class="da-form-item">
                                            <span class="formNote">Escriba un nombre descriptivo de la tarea</span>
                                            <input type="text" name="tareaname" value="<?php echo $tarea->nombre ?>"/>
                                        </div>
                                    </div>
                                    <div class="da-form-row">
                                        <label>Descripci&oacute;n</label>
                                        <div class="da-form-item">
                                            <span class="formNote">Escriba una descripci&oacute;n de la tarea</span>
                                            <textarea rows="auto" cols="auto" name="tareadesc"><?php echo $tarea->descripcion ?></textarea>
                                        </div>
                                    </div>
                                    <div class="da-button-row">
                                        <input type="hidden" name="tareaid" value="<?php echo $tarea->id ?>" />
                                        <input type="submit" value="Guardar" class="da-button green">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
