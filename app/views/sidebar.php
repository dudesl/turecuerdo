<?php

function sidebar($active) {
    switch ($_SESSION['user']->rol) {
        case "Administrador":
            return getAdminSidebar($active);
            break;
        case "Empleado":
            return getEmpleadoSidebar();
            break;
    }
}

function getAdminSidebar($active){
    ?>
    <!-- Sidebar -->
    <div id="da-sidebar">

        <!-- Main Navigation -->
        <div id="da-main-nav" class="da-button-container">
            <ul>
                <?php
                $tableroactive = "";
                if ($active == "tablero")
                    $tableroactive = "active";
                ?>
                <li class="<?php echo $tableroactive ?>">
                    <a href="?dashboard">
                        <!-- Icon Container -->
                        <span class="da-nav-icon">
                            <img src="images/icons/black/32/home.png" alt="Dashboard" />
                        </span>
                        Tablero
                    </a>
                </li>
                <?php
                $eventoactive = "";
                if ($active == "evento")
                    $eventoactive = "active";
                ?>
                <li class="<?php echo $eventoactive ?>">
                    <a>
                        <!-- Icon Container -->
                        <span class="da-nav-icon">
                            <img src="images/icons/black/32/day_calendar.png" alt="Calendar" />
                        </span>
                        Eventos
                    </a>
                    <ul>
                        <li><a href="?eventos">Ver eventos</a></li>
                        <li><a href="?eventos&add">Agregar evento</a></li>
                        <li><a href="?eventos&asignar">Asignar tareas a usuarios</a></li>
                    </ul>
                </li>
                <?php
                $servicioactive = "";
                if ($active == "servicio")
                    $servicioactive = "active";
                ?>
                <li class="<?php echo $servicioactive ?>">
                    <a>
                        <!-- Icon Container -->
                        <span class="da-nav-icon">
                            <img src="images/icons/black/32/table_1.png" alt="Table" />
                        </span>
                        Servicios
                    </a>
                    <ul>
                        <li><a href="?servicios">Ver servicios</a></li>
                        <li><a href="?servicios&add">Agregar servicio</a></li>
                    </ul>
                </li>
                <?php
                $tareaactive = "";
                if ($active == "tarea")
                    $tareaactive = "active";
                ?>
                <li class="<?php echo $tareaactive ?>">
                    <a href="?tareas">
                        <!-- Icon Container -->
                        <span class="da-nav-icon">
                            <img src="images/icons/black/32/create_write.png" alt="Form" />
                        </span>
                        Tareas
                    </a>
                </li>

                <?php
                $rrhhactive = "";
                if ($active == "rrhh")
                    $rrhhactive = "active";
                ?>
                <li class="<?php echo $rrhhactive ?>">
                    <a>
                        <!-- Icon Container -->
                        <span class="da-nav-icon">
                            <img src="images/icons/black/32/file_cabinet.png" alt="File Handling" />
                        </span>
                        Empleados
                    </a>
                    <ul>
                        <li><a href="?rrhh">Ver Empleados</a></li>
                        <li><a href="?rrhh&add">Agregar Empleado</a></li>
                    </ul>
                </li>                        
            </ul>
        </div>

    </div>
    <?php
}

function getEmpleadoSidebar() {
    ?>
    <!-- Sidebar -->
    <div id="da-sidebar">

        <!-- Main Navigation -->
        <div id="da-main-nav" class="da-button-container">
            <ul>
                <li class="active">
                    <a href="?dashboard">
                        <!-- Icon Container -->
                        <span class="da-nav-icon">
                            <img src="images/icons/black/32/home.png" alt="Dashboard" />
                        </span>
                        Menu
                    </a>
                    <ul>
                        <li><a href="?eventos">Ver Eventos</a></li>
                        <li><a href="?eventos&asignacion">Ver asignaciones</a></li>
                    </ul>
                </li>                                        
            </ul>
        </div>

    </div>
    <?php
}
?>

