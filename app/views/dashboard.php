             <div id="da-header-bottom">
                <!-- Container -->
                <div class="da-container clearfix">
                	
                    <!-- Breadcrumbs -->
                    <div id="da-breadcrumb">
                        <ul>
                            <li class="active"><span><img src="images/icons/black/16/home.png" alt="Home" />Tablero</span></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    
        <!-- Content -->
        <div id="da-content">
            
            <!-- Container -->
            <div class="da-container clearfix">
                
                <!-- Sidebar Separator do not remove -->
                <div id="da-sidebar-separator"></div>
                
                <?php sidebar('tablero')?>
                <!-- Main Content Wrapper -->
                <div id="da-content-wrap" class="clearfix">
                
                	<!-- Content Area -->
                	<div id="da-content-area">
                		<div class="grid_3">
                        	<div class="da-panel">
                            	<div class="da-panel-header">
                                	<span class="da-panel-title">
                                        <img src="images/icons/color/calendar_2.png" alt="" />
                                        Calendario de Eventos
                                    </span>
                                    
                                </div>
                                <div class="da-panel-content">
                                	
                                </div>
                            </div>
                        </div>
                    	<div class="grid_1">                        	
                        	<div class="da-panel-widget">
                                <h1>Acciones</h1>
                                <ul class="da-summary-stat">
                                	<li>
                                    	<a href="?eventos">
                                            <span class="da-summary-icon" style="background-color:#e15656;">
                                                <img src="images/icons/white/32/events.png" alt="" />
                                            </span>
                                            <span class="da-summary-text">
                                                <span class="label">Eventos</span>
                                            </span>
                                            <div class="clearfix"></div>
                                        </a>
                                    </li>
                                    <li>
                                    	<a href="?servicios">
                                            <span class="da-summary-icon" style="background-color:#a6d037;">
                                                <img src="images/icons/white/32/services.png" alt="" />
                                            </span>
                                            <span class="da-summary-text">
                                                <span class="label">Servicios</span>
                                            </span>
                                            <div class="clearfix"></div>
                                        </a>
                                    </li>
                                    <li>
                                    	<a href="?tareas">
                                            <span class="da-summary-icon" style="background-color:#ea799b;">
                                                <img src="images/icons/white/32/task.png" alt="" />
                                            </span>
                                            <span class="da-summary-text">
                                                <span class="value up"></span>                                        
                                                <span class="label">Tareas</span>
	                                        </span>
                                            <div class="clearfix"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
        
