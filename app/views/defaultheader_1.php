<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <!-- Viewport metatags -->
        <meta name="HandheldFriendly" content="true" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- iOS webapp metatags -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />

        <!-- iOS webapp icons -->
        <link rel="apple-touch-icon" href="touch-icon-iphone.html" />
        <link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad.html" />
        <link rel="apple-touch-icon" sizes="114x114" href="touch-icon-retina.html" />

        <!-- CSS Reset -->
        <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
        <!--  Fluid Grid System -->
        <link rel="stylesheet" type="text/css" href="css/fluid.css" media="screen" />
        <!-- Theme Stylesheet -->
        <link rel="stylesheet" type="text/css" href="css/dandelion.theme.css" media="screen" />
        <!--  Main Stylesheet -->
        <link rel="stylesheet" type="text/css" href="css/dandelion.css" media="screen" />
        <!-- Demo Stylesheet -->
        <!-- <link rel="stylesheet" type="text/css" href="css/demo.css" media="screen" /> -->

        <!-- jQuery JavaScript File -->
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>

        <!-- jQuery-UI JavaScript Files -->
        <script type="text/javascript" src="jui/js/jquery-ui-1.8.20.min.js"></script>
        <script type="text/javascript" src="jui/js/jquery.ui.timepicker.min.js"></script>
        <script type="text/javascript" src="jui/js/jquery.ui.touch-punch.min.js"></script>
        <link rel="stylesheet" type="text/css" href="jui/css/jquery.ui.all.css" media="screen" />

        <!-- Plugin Files -->

        <!-- FileInput Plugin -->
        <script type="text/javascript" src="js/jquery.fileinput.js"></script>
        <!-- Placeholder Plugin -->
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <!-- Mousewheel Plugin -->
        <script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
        <!-- Scrollbar Plugin -->
        <script type="text/javascript" src="js/jquery.tinyscrollbar.min.js"></script>
        <!-- Tooltips Plugin -->
        <script type="text/javascript" src="plugins/tipsy/jquery.tipsy-min.js"></script>
        <link rel="stylesheet" href="plugins/tipsy/tipsy.css" />

        <!-- Spinner Plugin -->
        <script type="text/javascript" src="jui/js/jquery.ui.spinner.min.js"></script>

        <!-- Validation Plugin -->
        <script type="text/javascript" src="plugins/validate/jquery.validate.min.js"></script>

        <!-- DataTables Plugin -->
        <script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>

        <!-- Chosen Plugin -->
        <script type="text/javascript" src="plugins/chosen/chosen.jquery.js"></script>
        <link rel="stylesheet" href="plugins/chosen/chosen.css" media="screen" />

        <!-- Core JavaScript Files -->
        <script type="text/javascript" src="js/core/dandelion.core.js"></script>
        <script type="text/javascript" src="js/core/dandelion.customizer.js"></script>

        <script type="text/javascript" src="js/custom.js"></script>

        <title>Tu Recuerdo Admin - Tablero</title>


    </head>

    <body>

        <!-- Main Wrapper. Set this to 'fixed' for fixed layout and 'fluid' for fluid layout' -->
        <div id="da-wrapper" class="fixed">

            <!-- Header -->
            <div id="da-header">

                <div id="da-header-top">

                    <!-- Container -->
                    <div class="da-container clearfix">

                        <!-- Logo Container. All images put here will be vertically centere -->
                        <div id="da-logo-wrap">
                            <div id="da-logo">
                                <div id="da-logo-img">
                                    <h1>
                                        <a href="?" style="color:white">
                                            Tu Recuerdo Panel
                                        </a>
                                    </h1>
                                </div>
                            </div>
                        </div>

                        <!-- Header Toolbar Menu -->
                        <div id="da-header-toolbar" class="clearfix">
                            <div id="da-user-profile">
                                <div id="da-user-info">
                                    <?php
                                    echo $_SESSION['user']->nombre
                                    ?>
                                    <span class="da-user-title"><?php echo $_SESSION['user']->rol ?></span>
                                </div>
                                <ul class="da-header-dropdown">
                                    <li class="da-dropdown-caret">
                                        <span class="caret-outer"></span>
                                        <span class="caret-inner"></span>
                                    </li>
                                    <li class="da-dropdown-divider"></li>
                                    <li><a href="?dashboard">Tablero</a></li>
                                    <li class="da-dropdown-divider"></li>
                                    <li><a href="?perfil">Perfil</a></li>
                                    <li><a href="?perfil&edit&password">Cambiar Contrase&ntilde;a</a></li>
                                </ul>
                            </div>
                            <div id="da-header-button-container">
                                <ul>                                    
                                    <li class="da-header-button logout">
                                        <a href="?dashboard&close">Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>